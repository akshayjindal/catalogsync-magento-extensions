// -----------------------------------------------------------------------------------
//  External Image Gallery
// -----------------------------------------------------------------------------------
Product.Gallery.addMethods({
    initialize : function(containerId, uploader, imageTypes, ImageSizes) {
        this.containerId = containerId, this.container = $(this.containerId);
        this.uploader = uploader;
        this.imageTypes = imageTypes;
        this.ImageSizes = ImageSizes;
        if (this.uploader) {
            this.uploader.onFilesComplete = this.handleUploadComplete
                    .bind(this);
        }


        // this.uploader.onFileProgress = this.handleUploadProgress.bind(this);
        // this.uploader.onFileError = this.handleUploadError.bind(this);

        this.images = this.sortImages(ImageSizes);

        this.imagesValues = this.getElement('save_image').value.evalJSON();
        this.template = new Template('<tr id="__id__" class="real-row-data preview __extraclass__">' + this
                .getElement('template').innerHTML + '</tr>', new RegExp(
                '(^|.|\\r|\\n)(__([a-zA-Z0-9_]+)__)', ''));
        this.templateExternal = new Template('<tr id="__id__" selfasociated="__self__" associated-data="__associated__" class="real-row-data preview __extraclass__">' + this
                .getElement('template_external').innerHTML + '</tr>', new RegExp(
                '(^|.|\\r|\\n)(__([a-zA-Z0-9_]+)__)', ''));

        this.imageExternalTemplate = new Template('<td rowspan="__associated_count__" class="cell-image">' + this
                .getElement('template_image').innerHTML + '</td>', new RegExp(
                '(^|.|\\r|\\n)(__([a-zA-Z0-9_]+)__)', ''));

        this.fixParentTable();
        this.updateImages();
        // varienGlobalEvents.attachEventHandler('moveTab', this.onImageTabMove
        //         .bind(this));
    },
    createImageRow : function(image) {
        var vars = Object.clone(image);
        vars.id = this.prepareId(image.file);
        if( image.is_external == 0  || image.is_external === undefined){
            vars.extraclass = "single-row";
            var html = this.template.evaluate(vars);
        }else{            
            var imageContent = '';
            vars.image_width = image.width;
            vars.image_height = image.height;

            if(vars.is_first){
                vars.associated = image.associated_id;
                vars.self = 1;
                vars.extraclass = "single-row";
                vars.extraclasslabel = "";
                vars.extraclassposition = "";
                vars.image_section = this.imageExternalTemplate.evaluate(vars);
            }else{

                vars.self = 0;
                vars.associated_id = vars.value_id;
                vars.associated = image.associated_id;
                vars.extraclass = "";
                vars.image_section = '';
                vars.extraclasslabel = "hide-input";
                vars.extraclassposition = "hide-input";
            }
            //vars.previewurl = image.file + "?" + new Date().getTime();
            vars.previewurl = image.file;
            if (image.file.substr(0,8) == '/http://' || image.file.substr(0,9) == '/https://') {
                vars.previewurl = vars.previewurl.substr(1);    
            }
            var html = this.templateExternal.evaluate(vars);
            html = html.replace('<td id="html-image-here"></td>', vars.image_section);

            //IE Tweaks
            html = html.replace('<TD id="html-image-here"></TD>', vars.image_section);
            html = html.replace('<td id=html-image-here></td>', vars.image_section);
            html = html.replace('<TD id=html-image-here></TD>', vars.image_section);
        }

        Element.insert(this.getElement('list'), {
            bottom :html
        });

        $(vars.id).select('input[type="radio"]').each(function(radio) {
            radio.observe('change', this.onChangeRadio);
        }.bind(this));
    },
    sortImages : function(ImageSizes) {
        // Sort Images on Product Groups
        var newSortOrder = [];
        var GroupsExternal = [];
        
        this.getElement('save').value.evalJSON().each( function(image, i) {
            if (image.is_external) {
                try {
                    if ( typeof GroupsExternal[image.associated_id] == 'undefined') {
                        GroupsExternal[image.associated_id] = [];
                    }
                    
                    var img = new Image();
                    
                    // fix the URL for preview if necessary 
                    if (image.url.substr(0,8) == '/http://' || image.url.substr(0,9) == '/https://') image.url = image.url.substr(1);
                    
                    // show image preview 
                    img.src = image.url;

                    var finalWidth = image.width;

                    if (typeof finalWidth === 'undefined'){
                        finalWidth = img.naturalWidth;
                    }

                    if(ImageSizes.enabled){
                        if( finalWidth >= ImageSizes.base[0] && finalWidth <= ImageSizes.base[1] ){
                            image['sort_arttr'] = 0;
                        }
                        if( finalWidth >= ImageSizes.small[0] && finalWidth <= ImageSizes.small[1] ){
                            image['sort_arttr'] = 1;
                        }
                        if( finalWidth >= ImageSizes.thumbnail[0] && finalWidth <= ImageSizes.thumbnail[1] ){
                            image['sort_arttr'] = 2;
                        }                        
                        if( finalWidth >= ImageSizes.zoom[0] ) {
                            image['sort_arttr'] = 3;
                        }
                    }else{
                        image['sort_arttr'] = finalWidth;
                    }   

                    GroupsExternal[image.associated_id][i] = image;
                } catch (e2) {
                    console.log(e2);
                }

            }else{
                newSortOrder.push(image);
            }
        });

        GroupsExternal.each( function(group, i) {
            
            group.sort(function(a,b) {
                return parseInt(b.sort_arttr,10) - parseInt(a.sort_arttr, 10);
            });
            var is_first = true;
            group.reverse();
            group.each( function(image, i) {
                if (is_first) {
                    is_first = false;
                    image.is_first = true;

                }else{
                    image.is_first = false;
                }
                newSortOrder.push(image);
            });            
        });

        return newSortOrder;
    },
    changeImageIndex : function(element ,file, ajaxUrl, imageType) {
        var isValid = false;
        if(element.value.replace(/^\s+|\s+$/g,"") == ''){
            alert('Please Insert a value');
            return;
        }

        var data;

        new Ajax.Request( ajaxUrl, {
          method: 'post',
          parameters: { url: element.value },
          asynchronous: false,
          onSuccess: function(transport) {
            jsonResponse=transport.responseText.evalJSON();
            
            if ( jsonResponse.result == 'success' ) {
                isValid = true;
                data = jsonResponse;
            }
          }
        });

        if (isValid) {

            

            var index = this.getIndexByFile(file);

            this.file2id[data.url] = index;

            this.getFileElement(file,'cell-file input').value = data.url;
            this.images[index].new_url = data.url;
            this.images[index].file = data.url;
            this.images[index].old_file = file;
            this.images[index].has_changed = 1;


            this.getElement('save').value = Object.toJSON(this.images);
            this.setProductImages(file);

            this.updateState(this.images[index].file);
            this.container.setHasChanges();

        }else{
            element.value = file;
            alert('The URL is not a valid image URL.');
            return false;  
        }
    },
    setProductImages : function(file) {
        $H(this.imageTypes)
                .each(
                    function(pair) {
                        if (file == "no_selection") {
                            if (this.getFileElement(file,
                                    'cell-' + pair.key + ' input').checked) {
                                this.imagesValues[pair.key] = (file == 'no_selection' ? null
                                        : file);
                            }
                        }else{
                            var index = this.getIndexByFile(file);
                            if (typeof index === 'undefined') {
                                index = this.getIndexByOldFile(file);
                            }
                            
                            file = this.images[index].file;

                            var imagevalue = '';
                            if (this.getFileElement(file,'cell-' + pair.key + ' input').checked) {
                                
                                if(typeof this.images[index].new_url === 'undefined'){
                                    imagevalue = (file == 'no_selection' ? null : file);
                                }else{
                                    imagevalue = this.imagesValues[pair.key] = (file == 'no_selection' ? null : this.images[index].new_url);
                                }

                                // image zoom attribute is different - the initial slash will not exist for the URL  
                                if (pair.key == 'image_zoom') document.getElementById('image_zoom').value = (imagevalue.substr(0,8) == '/http://' || imagevalue.substr(0,9) == '/https://' ? imagevalue.substr(1) : imagevalue); 
                                else this.imagesValues[pair.key] = imagevalue; 
                            }
                        }
                    }.bind(this));

        this.getElement('save_image').value = Object.toJSON($H(this.imagesValues));
    },
    setProductImageExternal : function(file) {
        $H(this.imageTypes)
                .each(
                    function(pair) {
                        var index = this.getIndexByFile(file);
                        if (typeof index === 'undefined') {
                            index = this.getIndexByOldFile(file);
                        }
                        
                        file = this.images[index].file;
                        var currentId = this.images[index].associated_id;
                        var imagevalue = '';

                        if (this.getFileElement(file,'cell-' + pair.key + ' input').checked) {
                            
                            if(typeof this.images[index].new_url === 'undefined'){
                                imagevalue = (file == 'no_selection' ? null : file);
                            }else{
                                imagevalue = this.imagesValues[pair.key] = (file == 'no_selection' ? null : this.images[index].new_url);
                            }

                            // image zoom attribute is different - the initial slash will not exist for the URL  
                            if (pair.key == 'image') {

                                this.imagesValues[pair.key] = imagevalue; 

                                if(this.ImageSizes.enabled){
                                    var zoom_file = '';
                                    
                                    for (var i = 0; i < this.images.length; i++) {
                                      if (currentId == this.images[i].associated_id) {
                                            if ( this.images[i].width >= parseInt(this.ImageSizes.zoom[0]) ) {
                                                zoom_file = this.images[i].file;
                                            }
                                        }  
                                    }

                                    if (!zoom_file) {
                                        zoom_file = imagevalue;
                                    }

                                    this.getFileElement(zoom_file,'cell-image_zoom input').checked = 1;

                                    this.imagesValues['image_zoom'] = zoom_file; 
                                     
                                }
                            } else{
                                this.imagesValues[pair.key] = imagevalue; 
                            } 
                        }
                    }.bind(this));

        this.getElement('save_image').value = Object.toJSON($H(this.imagesValues));
    },
    getIndexByOldFile : function(file) {
        var index;
        this.images.each( function(item, i) {
            if (item.old_file == file) {
                index = i;
            }
        });
        return index;
    },
    updateExternalImage : function(file) {
        var index = this.getIndexByFile(file);
        if (typeof index === 'undefined') index = this.getIndexByOldFile(file);
        
        this.images[index].label = this.getFileElement(file, 'cell-label input').value;
        this.images[index].position = this.getFileElement(file,'cell-position input').value;
        this.images[index].removed = (this.getFileElement(file,'cell-remove input').checked ? 1 : 0);
        this.images[index].disabled = (this.getFileElement(file,'cell-disable input').checked ? 1 : 0);
        var currentId = this.images[index].value_id;
        var currentLabel = this.images[index].label;
        var currentPosition = this.images[index].position;

        for (var i = 0; i < this.images.length; i++) {
          if (currentId == this.images[i].associated_id) {
                this.images[i].label = currentLabel;
                this.images[i].position = currentPosition;
            }  
        }
        
        this.getElement('save').value = Object.toJSON(this.images);
        
        this.updateState(this.images[index].file);
        this.container.setHasChanges();
    },
    updateSizeImage : function(value_id, element, file) {

        var i = new Image();
        i.src = element.src;
        element.width = i.width;

        var text = i.width + 'x' + i.height;

        var index = this.getIndexByFile(file);
        if (typeof index === 'undefined') index = this.getIndexByOldFile(file);

        this.images[index].width = i.width;
        this.images[index].height = i.height;



        if(this.ImageSizes.enabled){

            if( i.width >= this.ImageSizes.thumbnail[0] && i.width <= this.ImageSizes.thumbnail[1] ){
                text = text + '<br/> <strong>Thumbnail</strong>';
                this.images[index].type = 'Thumbnail';
            }
            if( i.width >= this.ImageSizes.small[0] && i.width <= this.ImageSizes.small[1] ){
                text = text + '<br/> <strong>Small</strong>';
                this.images[index].type = 'Small';
            }
            if( i.width >= this.ImageSizes.base[0] && i.width <= this.ImageSizes.base[1] ){
                text = text + '<br/> <strong>Base</strong>';
                this.images[index].type = 'Base';
            }
            if( i.width >= this.ImageSizes.zoom[0] ) {
                text = text + '<br/> <strong>Zoom</strong>';
                this.images[index].type = 'Zoom';
            }
        }
        //document.getElementById('external-image-load-' + value_id).innerHTML = text;
        
    },
    updateVisualisation : function(file) {
        if (file) {
            var image = this.getImageByFile(file);
            
            this.getFileElement(file, 'cell-label input').value = image.label;
            this.getFileElement(file, 'cell-position input').value = image.position;
            this.getFileElement(file, 'cell-remove input').checked = (image.removed == 1);
            this.getFileElement(file, 'cell-disable input').checked = (image.disabled == 1);
            
        	// check if the currently selected file for the attribute matches the gallery image - if so, select it - compansate for the full URLs initial slash differences    
            $H(this.imageTypes).each(function(pair) {
            	if ((this.imagesValues[pair.key] == file) || 
                    ((file.substr(0,8) == '/http://' || file.substr(0,9) == '/https://') && file == '/' + this.imagesValues[pair.key])) {
                    this.getFileElement(file, 'cell-' + pair.key + ' input').checked = true;   
                }
            }.bind(this));
            
            this.updateState(file);
        }
    }
});
CdnGallery_Admin = Class.create();
CdnGallery_Admin.prototype = {
    imageUploader: null,
    
    newIncrement: 0,
    idIncrement :1,
    containerId :'',
    container :null,    
    ajaxUrl : null,
    
    initialize : function(containerId , imageUploader, ajaxCheckUrl) {
        this.containerId = containerId, this.container = $(this.containerId);
        this.imageUploader = imageUploader;
        this.ajaxUrl = ajaxCheckUrl;
    },
    getFileElement : function(value_id, element) {
        var selector = '#' + this.prepareId(value_id) + ' .' + element;
        var elems = $$(selector);
        if (!elems[0]) {
            try {
                console.log(selector);
            } catch (e2) {
                alert(selector);
            }
        }

        return $$('#' + this.prepareId(value_id) + ' .' + element)[0];
    },
    prepareId : function(value_id) {
        return this.containerId + '-image-' + value_id;
    },
    addNewImageButton : function()
    {
    	value_id = 'new';
    	
        //get Inputs values
		BaseUrlEl      = this.getFileElement(value_id, 'cell-url-base input');
        SmallUrlEl     = this.getFileElement(value_id, 'cell-url-small input');
        ThumbnailUrlEl = this.getFileElement(value_id, 'cell-url-thumbnail input');
        ZoomUrlEl      = this.getFileElement(value_id, 'cell-url-zoom input');
        LabelEl        = this.getFileElement(value_id, 'cell-label-external input');
		
        //Check the basic Image required
        if( BaseUrlEl.value == '' ){
            alert('Base Image is required. Please fill this input.');
            return;
        }

        //Create data to do ajax
		data = {
			base_url:      BaseUrlEl.value,
            small_url:     SmallUrlEl.value,
            thumbnail_url: ThumbnailUrlEl.value,
            image_label:   LabelEl.value,
		};
        if (typeof ZoomUrlEl !== 'undefined') {
           data['zoom_url'] =  ZoomUrlEl.value;
        }
		
        //call function to check all images
		this.checkImageUrl( data , true );

        //reset values
        BaseUrlEl.value      = '';
        SmallUrlEl.value     = '';
        ThumbnailUrlEl.value = '';
        ZoomUrlEl.value      = '';
        LabelEl.value        = '';
    },
    checkImageUrl : function ( data , clear )
    {

        var isValid = false;
        new Ajax.Request( this.ajaxUrl , {
          method: 'post',
          parameters: { base_url: data.base_url, 
                        small_url: data.small_url, 
                        thumbnail_url: data.thumbnail_url, 
                        zoom_url: data.zoom_url, 
                        image_label: data.image_label },
          asynchronous: false,
          onSuccess: function(transport) {
            jsonResponse=transport.responseText.evalJSON();
            
            if ( jsonResponse.result == 'success' )
            {
                isValid = true;
                data = jsonResponse;
            }
          }
        });

        if (isValid) {
          this.addNewImageRow( data );
        }
        else{
            alert('The URL is not a valid image URL.');
            return false;
        }
        
    },
    addNewImageRow : function ( data )
    {
        //Base Image & Associated Images
        
        var position =  this.imageUploader.getNextPosition();
        var label = data.label;
        var uploaderImage = this.imageUploader;
        var is_first = true;
        var total_images = data.images.length;

        data.images.each( function(image) {
            var index = uploaderImage.getIndexByFile(image.url);
            if (typeof index === 'undefined') {
                index = uploaderImage.getIndexByOldFile(image.url);
            }

            if( !(typeof index === 'undefined') ){
                total_images = total_images-1;
            } 
        });

        data.images.each( function(image) {
            var index = uploaderImage.getIndexByFile(image.url);
            if (typeof index === 'undefined') {
                index = uploaderImage.getIndexByOldFile(image.url);
            }

            if(typeof index === 'undefined'){
                var newImage = {};
                if (is_first) {
                    newImage.associated_id = 1;
                    newImage.associated_count = total_images;
                    newImage.is_first = true;
                    is_first = false;
                }else{
                    newImage.associated_id = 1;
                    newImage.is_first = false;
                }
                newImage.url = image.url;
                newImage.file = image.url;
                newImage.src = image.url + (new Date().getTime());
                newImage.unique_id = new Date().getTime();
                newImage.label = label;
                newImage.position = position;
                if (image.type == 'base') {
                    newImage.disabled = 0;
                }else{
                    newImage.disabled = 1;
                }
                
                newImage.size = image.size;
                newImage.removed = 0;    
                newImage.is_external = 1;
                newImage.has_change = 0;
                uploaderImage.images.push(newImage);     
                uploaderImage.container.setHasChanges();
                uploaderImage.updateImages();  
            } 
        });
        
    }
};