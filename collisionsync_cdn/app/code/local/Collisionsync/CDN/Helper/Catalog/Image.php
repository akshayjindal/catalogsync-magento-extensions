<?php

require_once(Mage::getBaseDir('lib') . '/cloudinary/Cloudinary.php');

class Collisionsync_CDN_Helper_Catalog_Image extends Mage_Catalog_Helper_Image {

    protected $_cludonary;

	/**
     * Initialize Helper to work with Image
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeName
     * @param mixed $imageFile
     * @return Mage_Catalog_Helper_Image
     */
    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile=null)
    {   

        $this->_reset();
        $this->_setModel(Mage::getModel('catalog/product_image'));
        $this->_getModel()->setDestinationSubdir($attributeName);
        $this->setProduct($product);

        $this->setWatermark(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_image")
        );
        $this->setWatermarkImageOpacity(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_imageOpacity")
        );
        $this->setWatermarkPosition(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_position")
        );
        $this->setWatermarkSize(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_size")
        );

        if ($imageFile) {
            $this->setImageFile($imageFile);
        } else {
            // add for work original size
            //Validate if is an url and set the value
            if(!filter_var($product->getData($attributeName), FILTER_VALIDATE_URL) === FALSE){
                $this->setImageFile($product->getData($attributeName));
            }else{
                $this->_getModel()->setBaseFile($this->getProduct()->getData($this->_getModel()->getDestinationSubdir()));
            }

        }
        return $this;
    }


    /**
     * Return Image URL
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $model = $this->_getModel();


            // Cloudinary Enabled
            if(Mage::getStoreConfig('cscdn/cloudinary/cloudinary_enabled') == 1) {
                

                Cloudinary::config(array(
                    "cloud_name" => Mage::getStoreConfig('cscdn/cloudinary/cloud_name'),
                    "api_key" => Mage::getStoreConfig('cscdn/cloudinary/api_key'),
                    "api_secret" => Mage::getStoreConfig('cscdn/cloudinary/api_secret')
                ));

                $width = $model->getWidth();
                $height = null;
                if($model->getHeight()){
                    $height = $model->getHeight();
                }else{
                    $height = $width;
                }

                $file = null;
                if ($this->getImageFile()) {
                    $file = $this->getImageFile();
                } else {
                    $file = $this->getProduct()->getData($model->getDestinationSubdir());
                }

                $newUrl = Cloudinary::cloudinary_url($file, array("width" => $width, "height" => $height, "crop" => "fill"));
                
                $this->setImageFile($newUrl);

            }

            

            //validate if is URl and Return it
            if(!filter_var($this->getImageFile(), FILTER_VALIDATE_URL) === FALSE){
                $url = $this->getImageFile();
            }else{
            	// execute magento default resize
                if ($this->getImageFile()) {
                    $model->setBaseFile($this->getImageFile());
                } else {
                    $model->setBaseFile($this->getProduct()->getData($model->getDestinationSubdir()));
                }

                if ($model->isCached()) {
                    return $model->getUrl();
                } else {
                    if ($this->_scheduleRotate) {
                        $model->rotate($this->getAngle());
                    }

                    if ($this->_scheduleResize) {
                        $model->resize();
                    }

                    if ($this->getWatermark()) {
                        $model->setWatermark($this->getWatermark());
                    }

                    $url = $model->saveFile()->getUrl();
                }
            }

        } catch (Exception $e) {
            $url = Mage::getDesign()->getSkinUrl($this->getPlaceholder());
        }
        return $url;
    }

	/**
	 * Set current Image model - We override init to be able to contain a reference in the model to the product object
	 *
	 * @param Collisionsync_CDN_Model_Catalog_Product_Image $model
	 * @return Mage_Catalog_Helper_Image
	 */
	protected function setProduct($product)
	{
		// set product 
		$this->_product = $product;
		
		// pass a reference to product to the model object (it will be overriden by this extension as well)
		$this->_model->setProduct($product);

		// return reference to self 
		return $this;
	}
	
}
		