<?php
class Collisionsync_CDN_Helper_Data extends Mage_Core_Helper_Abstract {
    protected $_mapping = null;

    /*
     * Generate an array with URL mapping
     *
     * @return array
     */
    protected function _getMapping() {
        if($this->_mapping != null) {
            return $this->_mapping;
        }

        $mapping = explode("\n", Mage::getStoreConfig('cscdn/others/map'));

        $this->_mapping = array();

        foreach($mapping as $mappingNode) {
            if(empty($mappingNode)) {
                continue;
            }

            $mappingNode = explode('=>', $mappingNode);

            $this->_mapping[trim($mappingNode[0])] = trim($mappingNode[1]);
        }

        return $this->_mapping;
    }

    private function _applyMapping($path) {
        $mapping = $this->_getMapping();
        
        foreach($mapping as $search => $replace) {
            if(substr($path, 0, strlen($search)) == $search) {
                return $replace.substr($path, strlen($search));
            }
        }
    }

    private function _applyReverseMapping($path) {
        $mapping = $this->_getMapping();
        
        foreach($mapping as $replace => $search) {
            if(substr($path, 0, strlen($search)) == $search) {
                return $replace.substr($path, strlen($search));
            }
        }
    }

    /*
     * @param string $url
     * @return string
     */
    private function _stripProtocol($url) {
        return str_replace(array('http://', 'https://'), array('', ''), $url);
    }

    /*
     * @param string $url
     * @return string
     */
    private function _addProtocol($url) {
        if(empty($url)) {
            return $url;
        }

        if(Mage::app()->getStore()->isCurrentlySecure()) {
            return 'https://'.$url;
        }

        return 'http://'.$url;
    }

    /*
     * @param string $path
     * @param bool $reverse
     * @return string
     */
    public function getUrl($path, $reverse = false) {

        // If disabled return unchanged path
        if(Mage::getStoreConfig('cscdn/others/enabled') != 1) {
            return $path;
        }

        // Skip when in secure mode (HTTPS) and disabled in the admin panel
        if(Mage::app()->getStore()->isCurrentlySecure() && Mage::getStoreConfig('cscdn/others/https') != 1) {
            $path = $this->_stripProtocol($path);
            $path = $this->_addProtocol($path);

            return $path;
        }

        $path = $this->_stripProtocol($path);

        // local URL => CDN URL
        if(!$reverse) {
            $path = $this->_applyMapping($path);
        }
        // CDN URL => local URL
        else {
            $path = $this->_applyReverseMapping($path);
        }

        $path = $this->_addProtocol($path);

        return $path;
    }

    /*
     * @param string $path
     * @return string
     */
    public function getReverseUrl($path) {
        return $this->getUrl($path, true);
    }

    public function imgUrl($image, $size = '') {
        // If disabled return the default value
        if(Mage::getStoreConfig('cscdn/images/enabled') != 1) {
            return $image;
        }       

        if(strpos($image,'http://') !== false || strpos($image,'https://') !== false ):
            return $image;
        endif;

        // Add base path
        $url = 'http://'.Mage::getStoreConfig('cscdn/images/url').'catalog/product/';

        // Add prodocol
        if(Mage::app()->getStore()->isCurrentlySecure()) {
            // Skip when in secure mode (HTTPS) and disabled in the admin panel
            if(Mage::getStoreConfig('cscdn/images/https') != 1) {
                return $image;
            }

            $url = str_replace('http://','https://',$url);
        }

        if($size == ''):
            $url .= $image;
        else:
            $pieces = explode('/', $image);
            $url .= $pieces[0] . '/'.$size.'/'.$pieces[1];
        endif;
        return $url;
    }

    public function getCdnUrl(){
        return Mage::getStoreConfig('cscdn/images/url').'catalog/product/';
    }

    /* 
     * detects remote cdn images that do not exist in the file system and writes them to catalog_product_entity_media_gallery_cdn table  
     */
    public function detectCdnImages() { 

    	// drop and re-create cdn table  
    	$this->query("drop table if exists cdn_images");
    	$this->query("create table cdn_images (image_url varchar(300) not null primary key)");

    	// put the main images, small images and thumbnails in the CDN images table for now - we will remove local images later 
    	$this->query("
		insert ignore into cdn_images
		select distinct av.value
		from eav_attribute a
		join catalog_product_entity_varchar av on av.attribute_id = a.attribute_id
		join catalog_product_entity e on e.entity_id = av.entity_id
		where a.attribute_code in ('image','small_image','thumbnail')
		and   av.value != 'no_selection'
		");
    	
    	// put the gallery images in the CDN images table for now - we will remove local images later 
    	$this->query("
		insert ignore into cdn_images
		select distinct value
    	from catalog_product_entity_media_gallery
		");
    	
    	// now remove the local images from the CDN table 
    	$cdn_images = $this->query("select image_url from cdn_images where image_url not like '/http://%' and image_url not like '/https://%'")->fetchAll();
    	foreach ($cdn_images as $cdn_image) {
    		    		
    		// delete local image if it exists in the file system 
    		if (file_exists(Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . str_replace('/',DS,$cdn_image['image_url'])))
    			$this->query("delete from cdn_images where image_url = '" . mysql_escape_string($cdn_image['image_url']) . "'");
    	}

    	// setup index for image urls in main tables for faster operation if it does not exist
    	$indexExists = $this->query("show index from catalog_product_entity_media_gallery where key_name = 'cdn'")->fetchAll();
    	if (!$indexExists) $this->query("alter table catalog_product_entity_media_gallery add index cdn (value)");
    	$indexExists = $this->query("show index from catalog_product_entity_varchar where key_name = 'cdn'")->fetchAll();
    	if (!$indexExists) $this->query("alter table catalog_product_entity_varchar add index cdn (value)");
    }

    /* 
     * backup cdn images 
     */
    public function backupCdnImages($backupname) { 
    	
    	// detect the cdn images and write them to the cdn_images table first 
    	$this->detectCdnImages();
    	 
    	// backup gallery images
    	$backuptable = "catalog_product_entity_media_gallery_backup_{$backupname}"; 
    	$this->query("drop table if exists $backuptable");
    	$this->query("create table $backuptable like catalog_product_entity_media_gallery");
    	$this->query("insert into $backuptable select * from catalog_product_entity_media_gallery where value in (select image_url from cdn_images)");

    	// backup gallery images settings 
    	$backuptable = "catalog_product_entity_media_gallery_value_backup_{$backupname}"; 
    	$this->query("drop table if exists $backuptable");
    	$this->query("create table $backuptable like catalog_product_entity_media_gallery_value");
    	$this->query("insert into $backuptable select * 
    	from catalog_product_entity_media_gallery_value 
    	where value_id in (select value_id from catalog_product_entity_media_gallery where value in (select image_url from cdn_images))");

    	// backup image attributes 
    	$backuptable = "catalog_product_entity_varchar_backup_{$backupname}";
    	$this->query("drop table if exists $backuptable");
    	$this->query("create table $backuptable like catalog_product_entity_varchar");
    	$this->query("insert into $backuptable select v.*
    	from catalog_product_entity_varchar v 
    	join cdn_images i on v.value = i.image_url 
    	and   attribute_id in (
	    	select attribute_id
    		from eav_attribute a
    		join eav_entity_type et on et.entity_type_id = a.entity_type_id
    		where a.attribute_code in ('image', 'small_image', 'thumbnail', 'image_label', 'small_image_label', 'thumbnail_label')
    		and   et.entity_type_code = 'catalog_product'
    	)");
    }
    
    /* 
     * returns backup options for cdn images
     */
    public function getCdnImagesBackupOptions() { 
    	
    	// get the backup tables 
    	$backuptables = $this->query("show tables like 'catalog_product_entity_media_gallery_backup%'")->fetchAll(PDO::FETCH_NUM);

    	// process the backup tables and determine the backup names 
    	$backups = array();
    	foreach ($backuptables as $backuptable) { 
    		$backupname = str_replace('catalog_product_entity_media_gallery_backup_','',$backuptable[0]);
    		$backups[$backupname] = $backupname;
    	}
    	return $backups; 
    }    
    
    /*
     * delete cdn images
    */
    public function deleteCdnImages() {
    	 
    	// detect the cdn images and write them to the cdn_images table first
    	$this->detectCdnImages();
    
    	// delete cdn gallery images (and empty images) 
    	$this->query("
    	delete g, v
    	from catalog_product_entity_media_gallery g 
    	join catalog_product_entity_media_gallery_value v on v.value_id = g.value_id
    	join cdn_images i on i.image_url = g.value 
		");
    
    	// delete empty images 
    	$this->query("
    	delete g, v
    	from catalog_product_entity_media_gallery g 
    	join catalog_product_entity_media_gallery_value v on v.value_id = g.value_id
    	where g.value = ''  
		");
    
    	// delete cdn gallery images with no value ids as well 
    	$this->query("
    	delete g 
    	from catalog_product_entity_media_gallery g
    	join cdn_images i on i.image_url = g.value  
		");

    	// delete image attributes 
    	$this->query("
    	delete v
    	from catalog_product_entity_varchar v
    	join cdn_images i on i.image_url = v.value 
		where attribute_id in (
    		select attribute_id
    		from eav_attribute a
    		join eav_entity_type et on et.entity_type_id = a.entity_type_id
    		where a.attribute_code in ('image', 'small_image', 'thumbnail', 'image_label', 'small_image_label', 'thumbnail_label')
    		and   et.entity_type_code = 'catalog_product'
    	)");
    	
    	// delete additional images attribute altogether - we are deprecating it
    	$this->query("
    	delete v
    	from catalog_product_entity_varchar v
		where attribute_id in (
    		select attribute_id
    		from eav_attribute a
    		join eav_entity_type et on et.entity_type_id = a.entity_type_id
    		where a.attribute_code in ('hide_add_image')
    		and   et.entity_type_code = 'catalog_product'
    	)");
    }

    /*
     * restore cdn images
    */
    public function restoreCdnImages($backupname) {
    
    	// delete the images before restoring them 
    	$this->deleteCdnImages();
    
    	// restore cdn gallery images
    	$this->query("insert into catalog_product_entity_varchar select * from catalog_product_entity_varchar_backup_{$backupname}");
		$this->query("insert into catalog_product_entity_media_gallery select * from catalog_product_entity_media_gallery_backup_{$backupname}");
		$this->query("insert into catalog_product_entity_media_gallery_value select * from catalog_product_entity_media_gallery_value_backup_{$backupname}");
    }

    /* 
     * returns the database connection 
     */
    protected function conn() { 
    	
    	// return cached connection if exists 
    	if (isset($this->dbconn)) return $this->dbconn;
    	
    	// get the connection and cache it and return it  
    	$this->dbconn = Mage::getSingleton('core/resource')->getConnection('core_write');
    	return $this->dbconn; 
    }

    /* 
     * get a database connection and execute a query 
     */
    protected function query($sql) { 
		return $this->conn()->query($sql);    	
    }
    
    /* 
     * returns the base image attribute id 
     */
    protected function getBaseImageAttributeId() { 
    	
    	// return cached attribute id if we already have it 
    	if (isset($this->baseImageAttributeId)) return $this->baseImageAttributeId; 
    	
    	// get the base image attribute id and cache it and return it
    	$this->baseImageAttributeId = intval($this->query("select attribute_id from eav_attribute where attribute_code = 'image' and entity_type_id = " . $this->getProductTypeId())->fetchColumn(0));
    	return $this->baseImageAttributeId;
    }
    
    /* 
     * returns the small image attribute id 
     */
    protected function getSmallImageAttributeId() { 
    	
    	// return cached attribute id if we already have it 
    	if (isset($this->smallImageAttributeId)) return $this->smallImageAttributeId; 
    	
    	// get the small image attribute id and cache it and return it
    	$this->smallImageAttributeId = intval($this->query("select attribute_id from eav_attribute where attribute_code = 'small_image' and entity_type_id = " . $this->getProductTypeId())->fetchColumn(0));
    	return $this->smallImageAttributeId;
    }
    
    /* 
     * returns the thumbnail image attribute id 
     */
    protected function getThumbnailAttributeId() { 
    	
    	// return cached attribute id if we already have it 
    	if (isset($this->thumbnailAttributeId)) return $this->thumbnailAttributeId; 
    	
    	// get the thumbnail attribute id and cache it and return it
    	$this->thumbnailAttributeId = intval($this->query("select attribute_id from eav_attribute where attribute_code = 'thumbnail' and entity_type_id = " . $this->getProductTypeId())->fetchColumn(0));
    	return $this->thumbnailAttributeId;
    }
    
    /* 
     * returns the gallery image attribute id 
     */
    protected function getGalleryAttributeId() { 
    	
    	// return cached attribute id if we already have it 
    	if (isset($this->galleryAttributeId)) return $this->galleryAttributeId; 
    	
    	// get the gallery attribute id and cache it and return it
    	$this->galleryAttributeId = intval($this->query("select attribute_id from eav_attribute where attribute_code = 'media_gallery' and entity_type_id = " . $this->getProductTypeId())->fetchColumn(0));
    	return $this->galleryAttributeId;
    }
    
    /* 
     * returns the product entity type id  
     */
    protected function getProductTypeId() { 
    	
    	// return cached type id if we already have it 
    	if (isset($this->productTypeId)) return $this->productTypeId; 
    	
    	// get the product type id and cache it and return it
    	$this->productTypeId = intval($this->query("select entity_type_id from eav_entity_type where entity_type_code = 'catalog_product'")->fetchColumn(0));
    	return $this->productTypeId;
    }
    
    /* 
     * inserts an image to the gallery 
     */
    protected function insertGalleryImage($productId, $image, $exclude = false) { 

    	// trim the image just in case 
    	$image = trim($image);
    	 
    	// ignore empty images 
    	if ($image == '') return;  

    	// fix image path if needed
    	if ($image != '' && substr($image,0,1) != '/') $image = '/' . $image;
    	
    	// check if the image already exists
    	$valueId = intval($this->query("
    	select value_id 
    	from catalog_product_entity_media_gallery   
    	where entity_id = $productId
    	and   attribute_id = " . $this->getGalleryAttributeId() . "
    	and   value = '" . mysql_escape_string($image) . "'")->fetchColumn(0));
    	
    	// if the image does not exist, add it 
    	if ($valueId == 0) {  
    	
	    	// insert image to the gallery
	    	$this->query("
	    	replace into catalog_product_entity_media_gallery set 
	    	attribute_id = " . $this->getGalleryAttributeId() . ",
	    	entity_id = $productId,
	    	value = '" . mysql_escape_string($image) . "'");
	    	
	    	// get inserted value id 
	    	$valueId = $this->conn()->lastInsertId();
    	}  
    	
  		// set the image gallery values - disable if needed  
    	$this->query("
    	replace into catalog_product_entity_media_gallery_value set 
    	value_id = $valueId, 
    	store_id = 0, 
    	position = 0, 
    	disabled = " . ($exclude ? '1' : '0'));
    }
    
    /* 
     * import cdn images for a product 
     */
    public function importCdnImages($sku, $baseimage, $smallimage, $thumbnail, $gallery) { 

    	// trim all inputs just in case
    	$baseimage = trim($baseimage);
    	$smallimage = trim($smallimage);
    	$thumbnail = trim($thumbnail);
    	$gallery = trim($gallery);
    	 
    	// fix image paths if needed 
    	if ($baseimage != '' && substr($baseimage,0,1) != '/') $baseimage = '/' . $baseimage;  
    	if ($smallimage != '' && substr($smallimage,0,1) != '/') $smallimage = '/' . $smallimage;
    	if ($thumbnail != '' && substr($thumbnail,0,1) != '/') $thumbnail = '/' . $thumbnail;
    	
    	// get product entity id 
    	$productId = intval($this->query("select entity_id from catalog_product_entity where sku = '" . mysql_escape_string($sku) . "'")->fetchColumn(0));
    	if (!$productId) return 'Cannot find sku: ' . $sku;
    			    	
    	// get current product base image 
    	$currentbaseimage = $this->query("select value from catalog_product_entity_varchar where entity_id = $productId and attribute_id = " . $this->getBaseImageAttributeId())->fetchColumn(0);

    	// if base image exists for the product, it must be local image - only set image attributes when it's empty 
    	if (!$currentbaseimage || $currentbaseimage == 'no_selection') { 
    		
    		// set base image 
    		if ($baseimage) $this->query("replace into catalog_product_entity_varchar set store_id = 0, 
    		entity_type_id = " . $this->getProductTypeId() . ", 
    		entity_id = $productId, 
 			attribute_id = " . $this->getBaseImageAttributeId() . ", 
    		value = '" . mysql_escape_string($baseimage) . "'"); 

    		// set small image 
    		if ($smallimage) $this->query("replace into catalog_product_entity_varchar set store_id = 0, 
    		entity_type_id = " . $this->getProductTypeId() . ", 
    		entity_id = $productId, 
    		attribute_id = " . $this->getSmallImageAttributeId() . ", 
    		value = '" . mysql_escape_string($smallimage) . "'"); 

    		// set thumbnail 
    		if ($thumbnail) $this->query("replace into catalog_product_entity_varchar set store_id = 0,
    		entity_type_id = " . $this->getProductTypeId() . ",
    		entity_id = $productId,
			attribute_id = " . $this->getThumbnailAttributeId() . ",
    		value = '" . mysql_escape_string($thumbnail) . "'");

    		// add these images to the gallery (so that they can be managed in the backend) but exclude them 
    		$this->insertGalleryImage($productId, $baseimage, true);
    		$this->insertGalleryImage($productId, $smallimage, true);
    		$this->insertGalleryImage($productId, $thumbnail, true);
    		
    		// add the regular gallery images - do not exclude them 
    		if ($gallery) foreach ($gallery as $galleryimage) $this->insertGalleryImage($productId, $galleryimage);
    	}
    	// looks like a base image exists for the product - conclude that it must be a local image and do not touch it - add gallery images but exclude them 
    	else { 

    		// add new images to the gallery and exclude them 
    		$this->insertGalleryImage($productId, $baseimage, true);
    		$this->insertGalleryImage($productId, $smallimage, true);
    		$this->insertGalleryImage($productId, $thumbnail, true);
    		
    		// add the regular gallery images and exclude them
    		if ($gallery) foreach ($gallery as $galleryimage) $this->insertGalleryImage($productId, $galleryimage, true);
    	} 
    	
    	return 'Imported ' . $sku . ' ' . $baseimage . ' ' . $smallimage . ' ' . $thumbnail . ' ' . implode(',',$gallery); 
    }
    
}
