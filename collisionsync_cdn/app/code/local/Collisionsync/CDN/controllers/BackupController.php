<?php

/* 
 * cdn images backup controller 
 */
class Collisionsync_CDN_BackupController extends Mage_Adminhtml_Controller_Action {

	/*
	 * display backup form 
	 */
	public function indexAction() {
		$this->display();
	}
	
	/* 
	 * display backup form 
	 */
	protected function display($showform = true, $errormsg = '') { 

		$this->loadLayout();
		$this->_setActiveMenu('system/CDN');
		
		// show the backup form if requested  
		if ($showform) $this->_addContent($this->getLayout()->createBlock('collisionsync_cdn/images_backup'));
		
		// add success message if entered 
		if ($errormsg != '') $this->_addContent($this->getLayout()->createBlock('core/text')->setText("<h3>{$errormsg}</h3>"));
		
		// render the screen 
		$this->renderLayout();
	}
	
	/* 
	 * backup submit handler 
	 */
	public function backupAction() {

		// get the backup name
		$backupname = $this->getRequest()->getPost('backupname');
		
		// backup cdn images 
		Mage::helper('CDN')->backupCdnImages($backupname); 
		
		// show success 
		$this->display(false, 'CDN Images backup ' . $backupname . ' is successfully completed.');
	}

}