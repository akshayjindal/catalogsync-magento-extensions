<?php

class Collisionsync_CDN_Adminhtml_ImageController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
        //check if send method is post
       if ($this->getRequest()->isPost()) {
            $response  = array();
            $images = array();
            try {
                
                
                //Startin images validation
                if($this->isImage($this->getRequest()->getPost('base_url'))){
                    $images[] = array('url' => $this->getRequest()->getPost('base_url'), 'type' => 'base');
                    $response['result'] = 'success';
                }else{
                    $response['base'] = "error";
                    $response['result'] = 'error';
                }
                if($this->isImage($this->getRequest()->getPost('small_url'))){
                    $images[] = array('url' => $this->getRequest()->getPost('small_url'), 'type' => 'small');
                }
                if($this->isImage($this->getRequest()->getPost('thumbnail_url'))){
                    $images[] = array('url' => $this->getRequest()->getPost('thumbnail_url'), 'type' => 'thumbnail');
                }
                if($this->isImage($this->getRequest()->getPost('zoom_url'))){
                    $images[] = array('url' => $this->getRequest()->getPost('zoom_url'), 'type' => 'zoom');
                }

                $response['label'] = $this->getRequest()->getPost('image_label');
                foreach ($images as $key => $image) {
                    $response['images'][] = array('url' => $image['url'], 'type' => $image['type'] ); 
                }

            } catch (Exception $e) {
                $response['base'] = "error";
                $reponse['error'] = $e->getMessage();
            }
        }else{
        	$response['base'] = "error";
        }

        //send json response to evalute it.
        $this->getResponse()
                    ->clearHeaders()
                    ->setHeader('Content-Type', 'text/xml')
                    ->setBody(json_encode($response));
        
        
        return false;
    }

    // Validate Single Image Function (Edit Single Image)
    public function validateAction(){
       if ($this->getRequest()->isPost()) {
            $response  = array();
            try {
                
                if($this->isImage($this->getRequest()->getPost('url'))){
                    $response['url'] = $this->getRequest()->getPost('url');
                    $response['result'] = 'success';
                }else{
                    $response['result'] = 'error';
                    $response['message'] = 'Error with image loading';
                }
            } catch (Exception $e) {
                $response['base'] = "error";
                $reponse['error'] = $e->getMessage();
            }
        }else{
            $response['base'] = "error";
        }

        $this->getResponse()
                    ->clearHeaders()
                    ->setHeader('Content-Type', 'text/xml')
                    ->setBody(json_encode($response));
        
        
        return false;
    }

    //Check if given url is an image type
    private function isImage($url)
    {
        if (substr($url,0,8) == '/http://' || substr($url,0,9) == '/https://') {
            $url = substr($url,1); 
        } 

        $params = array('http' => array(
                      'method' => 'HEAD'
                   ));
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp){
            return false;  // Problem with url
        } 
        $meta = stream_get_meta_data($fp);
        if ($meta === false){
            fclose($fp);
            return false;  // Problem reading data from url
        }

        $wrapper_data = $meta["wrapper_data"];
        if(is_array($wrapper_data)){
          foreach(array_keys($wrapper_data) as $hh){
              if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image") // strlen("Content-Type: image") == 19 
              {
                fclose($fp);
                return true;
              }
          }
        }

        fclose($fp);
        return false;
    }


    // private function isImage($url){
    //     try {
    //         // if the url came with /http:// or /https://, eliminate the initial slash 
    //         if (substr($url,0,8) == '/http://' || substr($url,0,9) == '/https://') $url = substr($url,1); 
            
    //         //we are doing a HEAD request with CURLOPT_NOBODY to make it efficient 

    //         $ch = curl_init();
    //         curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt ($ch, CURLOPT_URL, $url);
    //         curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 2);
    //         curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0');
    //         curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
    //         curl_setopt ($ch, CURLOPT_HEADER, true); 
    //         curl_setopt ($ch, CURLOPT_NOBODY, true);
    //         curl_setopt ($ch, CURLOPT_ENCODING, 'gzip');

    //         $content = curl_exec ($ch);
    //         $contentType = (string) strtolower(curl_getinfo($ch, CURLINFO_CONTENT_TYPE));

    //         if ($contentType == 'image/gif' || $contentType == 'image/jpeg' || $contentType == 'image/png') {
    //             return true;
    //         }
    //     } catch (Exception $e) {
            
    //     }
    
    //     $res = 0;
    //     return false;
    // }
}