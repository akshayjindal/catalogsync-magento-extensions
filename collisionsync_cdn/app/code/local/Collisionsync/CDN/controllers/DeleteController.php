<?php

/* 
 * cdn images delete controller 
 */
class Collisionsync_CDN_DeleteController extends Mage_Adminhtml_Controller_Action {

	/*
	 * display delete form 
	 */
	public function indexAction() {
		$this->display();
	}
	
	/* 
	 * display delete form 
	 */
	protected function display($showform = true, $errormsg = '') { 

		$this->loadLayout();
		$this->_setActiveMenu('system/CDN');
		
		// show the delete form if requested  
		if ($showform) $this->_addContent($this->getLayout()->createBlock('collisionsync_cdn/images_delete'));
		
		// add success message if entered 
		if ($errormsg != '') $this->_addContent($this->getLayout()->createBlock('core/text')->setText("<h3>{$errormsg}</h3>"));
		
		// render the screen 
		$this->renderLayout();
	}
	
	/* 
	 * delete submit handler 
	 */
	public function deleteAction() {

		// confirm required 
		if (!$this->getRequest()->getPost('confirm')) { 
			$this->display(true, 'Please confirm that you want to delete CDN Images.');
			return;  
		}
		
		// delete cdn images 
		Mage::helper('CDN')->deleteCdnImages(); 
		
		// show success 
		$this->display(false, 'CDN Images are successfully deleted.');
	}

}