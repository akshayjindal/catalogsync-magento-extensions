<?php

/* 
 * cdn images import controller 
 */
class Collisionsync_CDN_ImportController extends Mage_Adminhtml_Controller_Action {

	/*
	 * display import form 
	 */
	public function indexAction() {
		$this->display();
	}
	
	/* 
	 * display import form 
	 */
	protected function display($showform = true, $errormsg = '', $html = '') { 

		$this->loadLayout();
		$this->_setActiveMenu('system/CDN');
		
		// show the import form if requested  
		if ($showform) $this->_addContent($this->getLayout()->createBlock('collisionsync_cdn/images_import'));
		
		// add success message if entered 
		if ($errormsg != '') $this->_addContent($this->getLayout()->createBlock('core/text')->setText("<h3>{$errormsg}</h3>"));

		// add html message if entered
		if ($html != '') $this->_addContent($this->getLayout()->createBlock('core/text')->setText($html));
		
		// render the screen 
		$this->renderLayout();
	}
	
	/* 
	 * import submit handler 
	 */
	public function importAction() {

		// check if we have the import file
		if(!isset($_FILES['importfile']['name']) || !file_exists($_FILES['importfile']['tmp_name'])) { 
			$this->display(true, 'No data uploaded. Please re-try.');
			return; 
		} 

		// uploaded import file in temporary folder - process it   
		$importfilepath = $_FILES['importfile']['tmp_name']; 

		// open the import file 
		$importfile = fopen($importfilepath, "r");
		if ($importfile === false) { 
			$this->display(true, 'Could not open uploaded file. Please re-try.');
			return;
		}
		
		// read the file and start processing it
		$results = '';
		$headerrow = fgetcsv($importfile, 2000, ","); // skip first row 
		while (($row = fgetcsv($importfile, 2000, ",")) !== false) {
			
			// assumed row fields 
			$sku = $row[0];
			$baseimage = $row[1];
			$smallimage = $row[2];
			$thumbnail = $row[3];
			$gallery = explode(',',$row[4]);

			// import the cdn images for the product 
			$results .= Mage::helper('CDN')->importCdnImages($sku, $baseimage, $smallimage, $thumbnail, $gallery) . '<br/>';
		} 

		// close the file and delete it - we don't need it anymore 
		fclose($importfile);
		unlink($importfilepath);
		
		// show results 
		$this->display(false, 'CDN Images are successfully imported.', $results);
	}

}