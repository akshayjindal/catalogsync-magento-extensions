<?php

/* 
 * cdn images restore controller 
 */
class Collisionsync_CDN_RestoreController extends Mage_Adminhtml_Controller_Action {

	/*
	 * display restore form 
	 */
	public function indexAction() {
		$this->display();
	}
	
	/* 
	 * display restore form 
	 */
	protected function display($showform = true, $errormsg = '') { 

		$this->loadLayout();
		$this->_setActiveMenu('system/CDN');
		
		// show the restore form if requested  
		if ($showform) $this->_addContent($this->getLayout()->createBlock('collisionsync_cdn/images_restore'));
		
		// add success message if entered 
		if ($errormsg != '') $this->_addContent($this->getLayout()->createBlock('core/text')->setText("<h3>{$errormsg}</h3>"));
		
		// render the screen 
		$this->renderLayout();
	}
	
	/* 
	 * restore submit handler 
	 */
	public function restoreAction() {

		// get the backup name
		$backupname = $this->getRequest()->getPost('backupname');
		
		// restore cdn images 
		Mage::helper('CDN')->restoreCdnImages($backupname); 
		
		// show success 
		$this->display(false, 'CDN Images are successfully restored from backup ' . $backupname . '.');
	}

}