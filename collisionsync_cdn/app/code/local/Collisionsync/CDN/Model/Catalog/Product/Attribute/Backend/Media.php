<?php
class Collisionsync_CDN_Model_Catalog_Product_Attribute_Backend_Media extends Mage_Catalog_Model_Product_Attribute_Backend_Media
{

	/* 
	 * override regular before save function for external images - otherwise the uploader may get an exception trying to move the file from temporary folder 
	 */
    public function beforeSave($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attrCode);
        if (!is_array($value) || !isset($value['images'])) {
            return;
        }

        if(!is_array($value['images']) && strlen($value['images']) > 0) {
           $value['images'] = Mage::helper('core')->jsonDecode($value['images']);
        }

        if (!is_array($value['images'])) {
           $value['images'] = array();
        }



        $clearImages = array();
        $newImages   = array();
        $existImages = array();
        if ($object->getIsDuplicate()!=true) {
            foreach ($value['images'] as &$image) {
                if(!empty($image['removed'])) {
                    $clearImages[] = $image['file'];
                } else if (!isset($image['value_id'])) {
                    
                	// external image uploaded 
                	if (isset($image['is_external']) && $image['is_external'] == 1) { 
                		
                		$image['new_file'] = $image['file'];
                		$newImages[$image['file']] = $image;
                	} 
                	// local image uploaded 
                	else {
                		$newFile = $this->_moveImageFromTmp($image['file']);
                		$image['new_file'] = $newFile;
                		$newImages[$image['file']] = $image;
                		$this->_renamedImages[$image['file']] = $newFile;
                		$image['file']             = $newFile;
                	} 
                } else {
                    $existImages[$image['file']] = $image;
                }
            }
        } else {
            // For duplicating we need copy original images.
            $duplicate = array();
            foreach ($value['images'] as &$image) {
                if (!isset($image['value_id'])) {
                    continue;
                }
                $duplicate[$image['value_id']] = $this->_copyImage($image['file']);
                $newImages[$image['file']] = $duplicate[$image['value_id']];
            }

            $value['duplicate'] = $duplicate;
        }

        foreach ($object->getMediaAttributes() as $mediaAttribute) {
            $mediaAttrCode = $mediaAttribute->getAttributeCode();
            $attrData = $object->getData($mediaAttrCode);

            if (in_array($attrData, $clearImages)) {
                $object->setData($mediaAttrCode, 'no_selection');
            }

            if (in_array($attrData, array_keys($newImages))) {
                $object->setData($mediaAttrCode, $newImages[$attrData]['new_file']);
                $object->setData($mediaAttrCode.'_label', $newImages[$attrData]['label']);
            }

            if (in_array($attrData, array_keys($existImages))) {
                $object->setData($mediaAttrCode.'_label', $existImages[$attrData]['label']);
            }
        }

        Mage::dispatchEvent('catalog_product_media_save_before', array('product' => $object, 'images' => $value));

        $object->setData($attrCode, $value);

        return $this;
    }

	public function afterSave($object)
    {
        parent::afterSave($object);
        
        //Get Attribute Code
        $attrCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attrCode);
        if (!is_array($value) || !isset($value['images']) || $object->isLockedAttribute($attrCode)) {
            return;
        }

        foreach ($value['images'] as &$image) {
            //if the image is already on the 
            if(isset($image['value_id'])) {
                //update external image if has changed
                if ( $image['is_external'] == 1 && $image['has_changed'] == 1) {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $query = "UPDATE `" . Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery') . "` SET `value`='" . $image['new_url'] . "' WHERE `value_id`=" . $image['value_id'] . ";";
                    $write->query($query);
                }
            }
        }
    }
}
		