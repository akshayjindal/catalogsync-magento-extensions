<?php

class Collisionsync_CDN_Model_Catalog_Product_Image extends Mage_Catalog_Model_Product_Image {

	// reference to product that this image belongs to  
	protected $_product; 

   	// save original image attribute value from the database - this may be redundant 
   	protected $imageAttributeValue;
	
    protected $_upbaseImage;

    public function getUrl() {
    	
    	// default standard url - this is the placeholder image if local image does not exist 
    	$localFile = parent::getUrl();
    	
        // if disabled return the default value
        if(Mage::getStoreConfig('cscdn/images/enabled') != 1) return $localFile;
        
        if(strpos($this->_upbaseImage, 'http')){
            $externalUrl = str_replace('/http', 'http', $this->_upbaseImage);
            return $externalUrl;
        }

         // Add base path
        $url = Mage::getStoreConfig('cscdn/images/url');

        // Add prodocol
        if(Mage::app()->getStore()->isCurrentlySecure()) {
            
            // Skip when in secure mode (HTTPS) and disabled in the admin panel
            if(Mage::getStoreConfig('cscdn/images/https') != 1) return $localFile;
            
            // add https 
            $url = str_replace('http://','https://',$url);
        }     

        // Add file path
        $url .= str_replace(Mage::getBaseDir('media'), '', $this->_newFile);

        $cdnimage=$this->_upbaseImage;
        
        // Check if local file exists and is not a placeholder
        $placeholder = strpos($localFile,'placeholder');
        
        // local file exists that client uploaded - use that  
        if (!$placeholder) return $localFile;

        // show the CDN image if it's not empty - we check with a special batch utility to see if it exists - it's up to us to check 
        // there was a check here that checked dynamically but it's affecting the performance (commented below) 
        if ($this->imageAttributeValue) return $cdnimage;
        
        // if there are no images, show the manufacturer logo when possible  
        $logo = $this->getManufacturerLogo($this->getImageSize('200'));
        if ($logo != '') return $logo;

        // no manufacturer logo, nothing - well, we give up - show placeholder
        return $localFile;
        
       	// check if the image exists in the CDN and use it if it does
       	//if ($this->checkRemoteFile($cdnimage)) return $cdnimage;
       	
        // return $this->_upbaseImage;
    }

    /* 
     * set reference to product 
     */
    public function setProduct($product) { 
		$this->_product = $product;     	
    }
    
    /*
     * returns the manufacturer logo based on the given size
    */
    public function getManufacturerLogo($size) {

    	// if manufacturer info is not loaded, resort to SQL - unfortunately after hours of searching I could not find a native way of loading attributes
    	// this will slow down the list pages a bit but the impact should be minimal 
    	if (!$this->_product->hasData('manufacturer')) {  

    		// get the manufacturer for the product
    		// ignore the store - if we were to have multiple stores and the manufacturer changed between them this could be a problem but for us it's very unlikely 
    		$manufacturer = Mage::getSingleton('core/resource')->getConnection('core_read')->query("
    		select aov.value 
			from eav_attribute a
			join catalog_product_entity_int cpa on cpa.attribute_id = a.attribute_id  
			join eav_attribute_option ao on ao.attribute_id = cpa.attribute_id and ao.option_id = cpa.value  
			join eav_attribute_option_value aov on aov.option_id = ao.option_id 
			where a.attribute_code = 'manufacturer'
			and   cpa.entity_id = '" . $this->_product->getEntityId() . "'")->fetchColumn();
    	}
    	// manufacturer is loaded (product detail screen)  
    	else { 
    	 
	    	// if the manufacturer info does not exist, nothing we can do  
	    	if (!$this->_product->getManufacturer()) return ''; 
    
	    	// get manufacturer name
	    	$manufacturer = $this->_product->getAttributeText('manufacturer');
    	} 
    	
    	// determine manufacturer logo from name
    	$code = $this->getManufacturerCode($manufacturer);
    	
    	// code not found - unknown manufacturer - nothing we can do
    	if ($code == '') return ''; 
    	
    	// return full image path
    	return "http://s3.amazonaws.com/industrialfiles/catalog/product/$code/$size/" . strtolower($code) . 'logo.jpg';
    }
    
    /*
     * determine manufacturer code from its attribute name
      */
     private function getManufacturerCode($manufacturer) {
	     switch ($manufacturer) {
		    case '3M': return '3M';
		    case 'Dynatron/Bondo Corp.': return '3M';
		    case 'MAR-HYDE PRODUCTS': return '3M';
		    case 'Accuspray': return '3M';
		    case 'Air Filtration Co.': return 'AFC';
			case 'ALC Sandy Jet': return 'ALC';
     		case 'Amflo Products': return 'AMF';
     		case 'Andrew Mack & Sons': return 'AMS';
     		case 'Angel Products': return 'ANG';
     		case 'Ansell': return 'ANS';
     		case 'Armstrong': return 'ARM';
     		case 'American Safety Razor': return 'ASR';
     		case 'Astro Pneumatic': return 'AST';
     		case 'Au-Ve-Co Products': return 'AVC';
     		case 'All Weld': return 'AWD';
     		case 'AXIS PAINTS': return 'AXP';
     		case 'Black & Decker': return 'BAD';
     		case 'Brady': return 'BDY';
     		case 'Blair Equipment': return 'BLR';
     		case 'ITW Binks': return 'BNK';
     		case 'Bosch': return 'BOS';
     		case 'BUFFALO RAGS': return 'BUF';
     		case 'CAMAIR': return 'CAM';
     		case 'Cans (empty)': return 'CAN';
     		case 'Caplugs': return 'CAP';
     		case 'Carborundum': return 'CAR';
     		case 'Channellock': return 'CLK';
     		case 'Clip Lizard': return 'CLZ';
     		case 'CRC': return 'CRC';
     		case 'Contec': return 'CTC';
     		case 'Catalyst Industries': return 'CTY';
     		case 'D-AV': return 'DAV';
     		case 'D-DAV': return 'DDV';
     		case 'DELL-CORNING CORP.': return 'DEL';
     		case 'ITW Devilbis': return 'DEV';
     		case 'Dent Fix Corp.': return 'DFX';
     		case 'Diamont': return 'DMT';
     		case 'Dynabrade': return 'DNB';
     		case 'Dynabrade Industrial': return 'DNB';
     		case 'DRESTER WASHERS': return 'DRS';
     		case 'DOMINION SURE SEAL': return 'DSS';
     		case 'Detro Mfg.': return 'DTO';
     		case 'DupliColor': return 'DUP';
     		case 'DIAMOND VOGEL DIRECT SHIP GOODS': return 'DVD';
     		case 'Dewalt Tools': return 'DWL';
     		case 'EAGLE MANUFACTURING COMPANY': return 'EGL';
     		case 'Elkay ': return 'ELK';
     		case 'Evercoat': return 'EVC';
     		case 'EXMix': return 'EZM';
     		case 'Eezer Products': return 'EZR';
     		case 'FBS DISTRIBUTION': return 'FBS';
     		case 'Ferro Industries': return 'FER';
     		case 'Frame Quip': return 'FMQ';
     		case 'Gerson': return 'GER';
     		case 'GLEnterprise': return 'GLE';
     		case 'Grow Automotive': return 'GRW';
     		case 'Granitize': return 'GRZ';
     		case 'Glasso': return 'GSO';
     		case 'HALDON CO.': return 'HAL';
     		case 'HOUSE OF KOLOR': return 'HOK';
     		case 'HERKULES EQUIPMENT': return 'HRK';
     		case 'Hutchins Manufacturing': return 'HTC';
     		case 'HI-TECH INDUSTRIES': return 'HTI';
     		case 'HYSTIK, INC.': return 'HYS';
     		case 'Intertape Polymer': return 'IPG';
     		case 'Infratech': return 'IRF';
     		case 'Irwin': return 'IRW';
     		case 'IWATA': return 'ITA';
     		case 'JACKCO TRANSNATIONAL': return 'JKO';
     		case 'JustRite': return 'JUS';
     		case 'Kimberly Clark': return 'KCP';
     		case 'Keysco Tools': return 'KEY';
     		case 'Kuhn Mfg.': return 'KHN';
     		case 'KARAJEN CORP.': return 'KJN';
     		case 'Kleanstrip (WM Barr)': return 'KLN';
     		case 'Krylon': return 'KRY';
     		case 'LakeCountry': return 'LCM';
     		case 'Lenox': return 'LEN';
     		case 'LIMCO': return 'LIM';
     		case 'LIKE 90': return 'LKE';
     		case 'Loctite': return 'LOC';
     		case 'LPS': return 'LPS';
     		case 'FUSOR PRODUCTS FROM LORD CORP.': return 'LRD';
     		case 'Makita': return 'MAK';
     		case 'Manufacturers mfg.': return 'MAN';
     		case 'Mo-Clamp/Pull-It': return 'MCP';
     		case 'Meguiars': return 'MEG';
     		case 'Microflex Gloves': return 'MFX';
     		case 'Morgan Mfg.': return 'MGN';
     		case 'Milwaukee': return 'MIL';
     		case 'Markal': return 'MKL';
     		case 'Morton Paint Co.': return 'MOR';
     		case 'Motor Guard  Corp.': return 'MRG';
     		case 'Mirka': return 'MRK';
     		case 'Master Appliance': return 'MTA';
     		case 'MWB Tack Rags': return 'MWB';
     		case 'MechanixWear': return 'MXW';
     		case 'National Detroit': return 'NAD';
     		case 'NEIKO TOOLS': return 'NKO';
     		case 'NORTON (SAINT-GOBAIN ABRASIVES)': return 'NOR';
     		case 'Nitro-Stan': return 'NTS';
     		case 'OMI-CRON': return 'OMI';
     		case 'One Shot Paints': return 'OST';
     		case 'Paragon': return 'PAR';
     		case 'Pacific Coast Lacquer': return 'PCL';
     		case 'Protective Industrial Products': return 'PIP';
     		case 'Pro Motorcar Products': return 'PMP';
     		case 'PolyPak': return 'POL';
     		case 'POR-15 Paints': return 'POR';
     		case 'PPC INC.': return 'PPC';
     		case 'Preval Sparyers': return 'PRE';
     		case 'PREVOST': return 'PRV';
     		case 'Presta Products': return 'PST';
     		case 'Protecto Trim': return 'PTC';
     		case 'Persyst Enterprises': return 'PYT';
     		case 'Rayovac': return 'RAY';
     		case 'RBL Products Inc.': return 'RBL';
     		case 'R and H Products': return 'RHP';
     		case 'Rivit': return 'RIV';
     		case 'READING TECHNOLOGIES INC.': return 'RTI';
     		case 'Rustoleum': return 'RUS';
     		case 'Standard Abrasives': return 'SAB';
     		case 'Samuel Strapping': return 'SAM';
     		case 'SATA': return 'SAT';
     		case 'Sealed Air': return 'SDA';
     		case 'Sem Products': return 'SEM';
     		case 'Schelgel Corp.': return 'SGL';
     		case 'S and G Tools Aid Corp.': return 'SGT';
     		case 'Shockwatch': return 'SHO';
     		case 'Sharpe Mfg.': return 'SHP';
     		case 'Style Line Corp.': return 'SLC';
     		case 'Stream Line Tools': return 'SLT';
     		case 'S.M. ARNOLD': return 'SMA';
     		case 'SUNMIGHT ABRASIVES': return 'SMT';
     		case 'SPRAY MAX': return 'SMX';
     		case 'Seymour of Sycamour': return 'SOS';
     		case 'Sprayway, Inc.': return 'SPY';
     		case 'Shamrock Mfg.': return 'SRK';
     		case 'Spray Sok Co. Inc.': return 'SSC';
     		case 'Stanley Proto': return 'STA';
     		case 'Stockhausen': return 'STK';
     		case 'STEEL WOOL': return 'STL';
     		case 'Spede Tool Mfg.': return 'STM';
     		case 'STEINER INDUSTRIES': return 'STN';
     		case 'Shoot Suit': return 'SUI';
     		case 'Survival Air Sales': return 'SVA';
     		case 'Tork': return 'TOR';
     		case 'TRISK SYSTEMS': return 'TSK';
     		case 'Transtar Products': return 'TST';
     		case 'TIME SAVER TOOLS': return 'TSV';
     		case 'Titan': return 'TTN';
     		case 'UniRam': return 'UNI';
     		case 'Uni-Spotter': return 'UNS';
     		case 'U-POL PRODUCTS': return 'UPL';
     		case 'Urethane Supply Co.': return 'URS';
     		case 'U.S. Chemicals': return 'USC';
     		case 'Viledon Filters': return 'VLD';
     		case 'VISKON AIR': return 'VSK';
     		case 'Valspar': return 'VSR';
     		case 'WANDA PAINT PRODUCTS': return 'WAN';
     		case 'Western Plastics': return 'WEP';
     		case 'Weiler': return 'WLR';
     		case 'Worthy Products': return 'WOR';
     		case 'Binding Source': return 'BND';
     		case 'Dupont': return 'DPT';
     		case 'Glue Dots': return 'GDT';
     		case 'Mapa Trilites': return 'TRI';
     		case 'MDI': return 'MDI';
     		case 'Stretch Wrap': return 'SWP';
     		case 'Auveco': return 'AVC';
     		case 'Dynabrade Inc.': return 'DNB';
     		case 'E-Z Mixing Cups': return 'EZM';
     		case 'GL Enterprises': return 'GLE';
     		case 'ITW Devilbiss': return 'DEV';
     		case 'Lord Corp.': return 'LRD';
     		case 'Louis M. Gerson Co.': return 'GER';
     		case 'Makita U.S.A.': return 'MAK';
     		case 'Preval Sprayers': return 'PRE';
     		case 'ProtektoTrim': return 'PTC';
     		case 'S & G Tools Aid Corp.': return 'SGT';
     		case 'SATA Spray Equip.': return 'SAT';
     		case 'SprayMax': return 'SMX';
     		case 'Stanley Industrial': return 'STA';
     		case 'Tork Wiping': return 'TOR';
     		case 'U.S. Chemical': return 'USC';
     		case 'Uni-Ram Corp.': return 'UNI';
     		case 'Viskon-Aire': return 'VSK';
     		default: return '';
	    }
	    return '';
    }
    /* 
     * returns image size based on destination sub directory 
     */
    private function getImageSize($default='original') { 
            
    	switch($this->getDestinationSubdir()) 
    	{ 
    		case 'small_image': return '145'; break;
    		case 'image': return '250'; break;
    		case 'thumbnail': return '50'; break;
    		default: return $default; break; 
    	} 
    }

    /**
     * Set filenames for base file and new file
     *
     * @param string $file
     * @return Mage_Catalog_Model_Product_Image
     */
    public function setBaseFile($file)
    {   
        // $originalFile = $file;
        $localFile = parent::getUrl();
        $oldFile = $file;
        $this->_isBaseFilePlaceholder = false;

        // save original image attribute value from the database - this may be redundant 
        $this->imageAttributeValue = ($oldFile == 'no_selection' ? '' : $oldFile);
        
        $width = $this->getImageSize();

        $pieces = explode('/', $oldFile);
        $filenewN = '/' .$pieces[1] . '/'.$width.'/' . $pieces[2];
        
        //Append protocol
        if(Mage::app()->getStore()->isCurrentlySecure())
            $remoteUrl = 'https://';
        else
            $remoteUrl = 'http://';

        $remoteUrl .=  Mage::getStoreConfig('cscdn/images/url') . 'catalog/product' . trim($filenewN);

        if(strpos($file,'http'))
            $this->_upbaseImage = $oldFile;
        else   
            $this->_upbaseImage = $remoteUrl;

        if (($file) && (0 !== strpos($file, '/', 0))) {
            $file = '/' . $file;
        }
        $baseDir = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath();

        if ('/no_selection' == $file) {
            $file = null;
        }
        if ($file) {
            if ((!$this->_fileExists($baseDir . $file)) || !$this->_checkMemory($baseDir . $file)) {
                $file = null;
            }
        }

        if (!$file) {
            // check if placeholder defined in config
            $isConfigPlaceholder = Mage::getStoreConfig("catalog/placeholder/{$this->getDestinationSubdir()}_placeholder");
            $configPlaceholder   = '/placeholder/' . $isConfigPlaceholder;
            if ($isConfigPlaceholder && $this->_fileExists($baseDir . $configPlaceholder)) {
                $file = $configPlaceholder;
            }
            else {
                // replace file with skin or default skin placeholder
                $skinBaseDir     = Mage::getDesign()->getSkinBaseDir();
                $skinPlaceholder = "/images/catalog/product/placeholder/{$this->getDestinationSubdir()}.jpg";
                $file = $skinPlaceholder;
                if (file_exists($skinBaseDir . $file)) {
                    $baseDir = $skinBaseDir;
                }
                else {
                    $baseDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default'));
                    if (!file_exists($baseDir . $file)) {
                        $baseDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default', '_package' => 'base'));
                    }
                }
            }
            $this->_isBaseFilePlaceholder = true;
        }

        $baseFile = $baseDir . $file;

        if ((!$file) || (!file_exists($baseFile))) {
            //throw new Exception(Mage::helper('catalog')->__('Image file was not found.'));
        }

        $this->_baseFile = $baseFile;
        // build new filename (most important params)
        $path = array(
            Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath(),
            'cache',
            Mage::app()->getStore()->getId(),
            $path[] = $this->getDestinationSubdir()
        );
        if((!empty($this->_width)) || (!empty($this->_height)))
            $path[] = "{$this->_width}x{$this->_height}";

        // add misk params as a hash
        $miscParams = array(
                ($this->_keepAspectRatio  ? '' : 'non') . 'proportional',
                ($this->_keepFrame        ? '' : 'no')  . 'frame',
                ($this->_keepTransparency ? '' : 'no')  . 'transparency',
                ($this->_constrainOnly ? 'do' : 'not')  . 'constrainonly',
                $this->_rgbToString($this->_backgroundColor),
                'angle' . $this->_angle,
                'quality' . $this->_quality
        );

        // if has watermark add watermark params to hash
        if ($this->getWatermarkFile()) {
            $miscParams[] = $this->getWatermarkFile();
            $miscParams[] = $this->getWatermarkImageOpacity();
            $miscParams[] = $this->getWatermarkPosition();
            $miscParams[] = $this->getWatermarkWidth();
            $miscParams[] = $this->getWatermarkHeigth();
        }

        $path[] = md5(implode('_', $miscParams));

        // append prepared filename
        $this->_newFile = implode('/', $path) . $file; // the $file contains heading slash

        return $this;
    }

    public function checkRemoteFile($url){

        $placeholder = strpos($url,'placeholder');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE && !$placeholder):
            curl_close($ch);
            return $url;
        else:
            curl_close($ch);
            return false;
        endif;
    }
}
