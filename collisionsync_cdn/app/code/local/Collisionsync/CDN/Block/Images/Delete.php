<?php

/**
 * CDN Images Delete block  
 */

class Collisionsync_CDN_Block_Images_Delete extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
    	parent::__construct();
        
        $this->_objectId = 'collisionsync_delete_form';
        $this->_blockGroup = 'collisionsync_cdn';
        $this->_controller = 'images';
        $this->_mode = 'delete';
    	
        $this->_updateButton('save', 'label', 'Delete CDN Images');
        $this->_updateButton('save', 'onclick', 'delete_form.submit();');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    public function getHeaderText()
    {
        return 'Delete CDN Images';
    }
}
