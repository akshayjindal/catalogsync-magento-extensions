<?php

/**
 * CDN Images Backup block  
 */

class Collisionsync_CDN_Block_Images_Backup extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
    	parent::__construct();
        
        $this->_objectId = 'collisionsync_backup_form';
        $this->_blockGroup = 'collisionsync_cdn';
        $this->_controller = 'images';
        $this->_mode = 'backup';
    	
        $this->_updateButton('save', 'label', 'Backup CDN Images');
        $this->_updateButton('save', 'onclick', 'backup_form.submit();');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    public function getHeaderText()
    {
        return 'Backup CDN Images';
    }
}
