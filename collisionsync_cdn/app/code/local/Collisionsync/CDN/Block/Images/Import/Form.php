<?php

/**
 * CDN Images import form 
 */
class Collisionsync_CDN_Block_Images_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{     
    protected function _prepareForm()
    {
    	$form = new Varien_Data_Form(array(
			'id' =>	'import_form',
    		'action' =>	$this->getUrl('collisionsync_cdn/import/import'),
    		'method' =>	'post',
   			'enctype' => 'multipart/form-data'
    	));

    	$form->addField('explanation', 'label', array('value' => 'Please upload the import file in CSV format. First row is assumed to be header and will be skipped.'));
    	$form->addField('fileformat', 'label', array('value' => 'File Format: SKU, base image, small image, thumbnail, gallery images (comma separated in a single column)'));
    	$form->addField('importfile', 'file', array('label' => 'Import File', 'name' => 'importfile', 'value' => 'Upload', 'required' => true));
    	
        $form->setUseContainer(true);

        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
