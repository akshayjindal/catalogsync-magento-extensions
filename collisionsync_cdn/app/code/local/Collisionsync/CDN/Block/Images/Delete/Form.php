<?php

/**
 * CDN Images delete form 
 */
class Collisionsync_CDN_Block_Images_Delete_Form extends Mage_Adminhtml_Block_Widget_Form
{     
    protected function _prepareForm()
    {
    	$form = new Varien_Data_Form(array(
			'id'	=>	'delete_form',
    		'action'=>	$this->getUrl('collisionsync_cdn/delete/delete'),
    		'method'=>	'post'
    	));

		$form->addField('confirmlabel', 'label', array(
			'value' => 'Are you sure you want to delete the CDN images? Local images will remain intact.'
		));

    	$form->addField('confirm', 'checkbox', array(
			'name' => 'confirm',
			'label'	=> 'Confirm Image Deletion',
			'value' => 1, 
    		'required' => true
		));

        $form->setUseContainer(true);

        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
