<?php

/**
 * CDN Images Import block  
 */

class Collisionsync_CDN_Block_Images_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
    	parent::__construct();
        
        $this->_objectId = 'collisionsync_import_form';
        $this->_blockGroup = 'collisionsync_cdn';
        $this->_controller = 'images';
        $this->_mode = 'import';
    	
        $this->_updateButton('save', 'label', 'Import CDN Images');
        $this->_updateButton('save', 'onclick', 'import_form.submit();');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    public function getHeaderText()
    {
        return 'Import CDN Images';
    }
}
