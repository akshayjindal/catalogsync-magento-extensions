<?php

/**
 * CDN Images Restore block  
 */

class Collisionsync_CDN_Block_Images_Restore extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
    	parent::__construct();
        
        $this->_objectId = 'collisionsync_restore_form';
        $this->_blockGroup = 'collisionsync_cdn';
        $this->_controller = 'images';
        $this->_mode = 'restore';
    	
        $this->_updateButton('save', 'label', 'Restore CDN Images');
        $this->_updateButton('save', 'onclick', 'restore_form.submit();');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    public function getHeaderText()
    {
        return 'Restore CDN Images';
    }
}
