<?php

/**
 * CDN Images restore form 
 */
class Collisionsync_CDN_Block_Images_Restore_Form extends Mage_Adminhtml_Block_Widget_Form
{     
    protected function _prepareForm()
    {
    	$form = new Varien_Data_Form(array(
			'id'	=>	'restore_form',
    		'action'=>	$this->getUrl('collisionsync_cdn/restore/restore'),
    		'method'=>	'post'
    	));

    	$form->addField('backupname', 'select', array(
                'label' => 'Backup To Restore From',
    			'name' => 'backupname',
                'required' => true,
    			'options' => Mage::helper('CDN')->getCdnImagesBackupOptions(),
            )
        );

        $form->setUseContainer(true);

        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
