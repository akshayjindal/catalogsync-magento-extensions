<?php

/**
 * CDN Images backup form 
 */
class Collisionsync_CDN_Block_Images_Backup_Form extends Mage_Adminhtml_Block_Widget_Form
{     
    protected function _prepareForm()
    {
    	$form = new Varien_Data_Form(array(
			'id'	=>	'backup_form',
    		'action'=>	$this->getUrl('collisionsync_cdn/backup/backup'),
    		'method'=>	'post'
    	));

        $form->addField('backupname', 'text', array(
                'name' => 'backupname',
                'label' => 'Backup Name',
                'required' => true,
            )
        );

        $form->setValues(array('backupname' => date('Ymd')));
        $form->setUseContainer(true);

        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
