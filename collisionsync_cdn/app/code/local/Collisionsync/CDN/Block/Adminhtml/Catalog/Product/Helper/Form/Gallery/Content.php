<?php

class Collisionsync_CDN_Block_Adminhtml_Catalog_Product_Helper_Form_Gallery_Content extends Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery_Content
{
	public function __construct()
    {
        parent::__construct();

        // change template file
        $this->setTemplate('collisionsync/cdn/catalog/product/helper/gallery.phtml');
    }

    /*
     * Override media attributes - add image_zoom attribute if enabled 
     */
    public function getMediaAttributes()
    {
    	// get regular image attributes 
    	$attributes = $this->getElement()->getDataObject()->getMediaAttributes();

    	// add image_zoom attribute if enabled 
    	if (Mage::getStoreConfigFlag('cscdn/images/zoom')) $attributes['image_zoom'] = $this->getElement()->getDataObject()->getResource()->getAttribute('image_zoom'); 

    	// return the image attributes array
    	return $attributes; 
    }

    /* 
     * returns the images of the product (to be constructed as a table by JS)  
     */
    public function getImagesJson()
    {
    	// no value found for the gallery attribute - return empty array 
        if(!is_array($this->getElement()->getValue())) return '[]'; 
        	
        // no images found in the gallery - return empty array 
        $value = $this->getElement()->getValue();
        if(count($value['images']) == 0) return '[]';
                
        // external images array that contains grouped external images 
        $externalImages = array();

        // external images waiting to be grouped (with same label)   
        $externalGroups = array();
                
        // local images
        $localImages = array();
                
        // CDN url root to be prepended to the external images 
        $cdnurl = Mage::helper('CDN')->getCdnUrl();

        // pre-sort images by the label, sort order and URL - this helps with viewing the selected images for base, small image, etc. - otherwise it may get confusing when the order changes (it's sorted again in JS, though) 
        uasort($value, function($a, $b) { if ($a['label'] < $b['label']) return -1; if ($a['label'] > $b['label']) return 1; if ($a['position'] < $b['position']) return -1; if ($a['position'] > $b['position']) return 1; if ($a['file'] < $b['file']) return -1; if ($a['file'] > $b['file']) return 1; return 0; });
        
        //group images based on label
        foreach ($value['images'] as $k => &$image) {
                	
        	// CDN module is not enabled - treat all images like local 
            if (!Mage::getStoreConfigFlag('cscdn/images/enabled')) {
            	$image['is_external'] = false;
                $image['url'] = Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']);
                $localImages[] = $image;
                continue; 
            }

            // check if the file exists on the server in the media folder - if it does, it's definitely a local file - output as-is 
            if (file_exists(Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . $image['file'] )){

                $image['is_external'] = false;
                $image['url'] = Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']);
                $localImages[] = $image;
                continue; 
            } 

            // since the file does not exist this must be either a bad local image or an external image - treat it as external image
            $image['is_external'] = true;
                    
            // if the image URL starts with http:// or https:// it must be an external image with full URL - add without initial slash for display  
            if (substr($image['file'], 0, 7) == 'http://' || substr($image['file'],0,8) == 'https://' || 
            	substr($image['file'], 0, 8) == '/http://' || substr($image['file'], 0, 9) == '/https://') {
                    	
                // eliminate initial slash when outputting this full URL kind of external image to be able to view it properly 
                // if (substr($image['file'], 0, 8) == '/http://' || substr($image['file'], 0, 9) == '/https://') $image['file'] = substr($image['file'], 1);
                    	
                // URL is the same thing as file (except for the initial slash maybe)
                $image['url'] = $image['file'];

            } 
            // if there are only 2 slashes in the URL, add the size folder to the URL automatically (collisionsync image URL standard)
            // use medium size (145 pixels) - if it doesn't resolve, it will be an invalid image - we tried our best
            elseif (substr_count($image['file'], '/') == 2){
            	$pieces = explode('/', $image['file']);
                $image['url'] = "http://" . $cdnurl . $pieces[1] . "/145/" . $pieces[2];
            }
            // otherwise just append the file to the main CDN url 
            else { 
            	$image['url'] = "http://" . $cdnurl . $image['file'];
            }
                   	
            // now add the image to the group
            if (!isset($externalGroups[$image['label']])) $externalGroups[$image['label']] = array($image); 
            else $externalGroups[$image['label']][] = $image;
        }
                
        //start to combine images on a simgle entity (single row)
        $associatedId = '';
        foreach ($externalGroups as $group) {
        	$i = 0;
            $associatedImagesCount = count($group);
            foreach ($group as $image) {

            	// make sure the image is not cached - append a unique id to the url 
                $image['src'] = $image['url'] . '?unique=' . uniqid();
                        
                // add a unique id to the image for JS functions
                $image['unique_id'] = uniqid();

                // get the first image in the group to show as the main image  
                if ($i == 0) $associatedId = $image['value_id'];
                $i++;
                        
                // this will be set from the first image in the group 
                $image['associated_id'] = $associatedId;
                $image['associated_count'] = $associatedImagesCount;
                        
                // add image to the array to be sent  
                $externalImages[] = $image;
            }
        }

        // now combine the external and local images in single JSON array 
        return Mage::helper('core')->jsonEncode(array_merge($externalImages, $localImages));
    }

    public function getImagesDetectionSize(){
        $size = array();
        $isEnabled = Mage::getStoreConfigFlag('cscdn/images/image_size_detection');
        $size['enabled'] = $isEnabled;
        if($isEnabled){
            $size['thumbnail'] = explode('-', Mage::getStoreConfig('cscdn/images/width_size_thumbnail'));
            $size['small'] = explode('-', Mage::getStoreConfig('cscdn/images/width_size_small'));
            $size['base'] = explode('-', Mage::getStoreConfig('cscdn/images/width_size_base'));
            $size['zoom'] = explode('-', Mage::getStoreConfig('cscdn/images/width_size_zoom'));
        }

        return Mage::helper('core')->jsonEncode($size);
    }
}