This repository contains the [Open Source Magento Extensions used with CatalogSync](http://catalogsync.com).

![catalog2.png](https://bitbucket.org/repo/a9kg58/images/4005796220-catalog2.png)

# CONTENT ONBOARDING #

CatalogSync can collect product content from a variety of data sources and import the data using API’s developed especially for Magento. These services are most valuable when customers need content extracted from a plurality of data sources and file structures.

# CONTENT SYNCHRONIZATION #

CatalogSync can synchronize product content in Magento with select product information management systems or other data structures to ensure information is up to date in different systems. Synchronization with platforms can be scheduled on a frequency or handled on an ad-hoc basis.

# FAST IMPORTS #

Our team has years of experience managing product content imports for Magento Community and Enterprise platforms. We have applied that experience toward developing a solution that significantly increases the speed of product data imports and eliminates common errors experienced when adding new data.

# CatalogSync reduces time spent on complex product content import processes by automating updates to Magento! #

* On-board product content from supplier and ERP systems such as SAP, NetSuite, or MS Dynamics
* Deliver enriched product content from product information management systems such as Salsify to Magento Community and Enterprise Editions on a regular frequency to keep content fresh
* Merge attributes and categories from multiple data sources into a single taxonomy.
* Cleanse product content prior to on-boarding into Magento without spending a lot of time editing spreadsheets!
* Our service can update 500,000 products with rich content in 90 minutes or less!