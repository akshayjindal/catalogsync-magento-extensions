/**
 * @category    Collisionsync
 * @package     CollisionsyncPro_FilterAjax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */
// the following function wraps code block that is executed once this javascript file is parsed. Lierally, this 
// notation says: here we define some anonymous function and call it once during file parsing. THis function has
// one parameter which is initialized with global jQuery object. Why use such complex notation: 
//      a.  all variables defined inside of the function belong to function's local scope, that is these variables
//          would not interfere with other global variables.
//      b.  we use jQuery $ notation in not conflicting way (along with prototype, ext, etc.)
(function($) {
    //region URL interception API

    var _ajaxifiedUrlOptions = {};
    var _lastAjaxActionSource = null;
    var _urlAjaxActions = {};
    $.mInterceptUrls = function(action, options) {
        _ajaxifiedUrlOptions[action] = $.extend({
            exactUrls: {},
            partialUrls: {},
            urlExceptions: {},
            callback: function(url, element, action, selectors) {}
        }, options);
    };
    $.mStopInterceptingUrls = function(action) {
        delete _ajaxifiedUrlOptions[action];
    };

    function _urlAjaxAction(locationUrl) {
        if (_urlAjaxActions[locationUrl] === undefined) {
            var locationAction = false;
            for (var action in _ajaxifiedUrlOptions) {
                var handled = false;
                $.each(_ajaxifiedUrlOptions[action].exactUrls, function (urlIndex, url) {
                    if (!handled && locationUrl == url) {
                        var isException = false;
                        $.each(_ajaxifiedUrlOptions[action].urlExceptions, function (urlExceptionIndex, urlException) {
                            if (!isException && locationUrl.indexOf(urlException) != -1) {
                                isException = true;
                            }
                        });
                        if (!isException) {
                            handled = true;
                            locationAction = action;
                        }
                    }
                });
                $.each(_ajaxifiedUrlOptions[action].partialUrls, function (urlIndex, url) {
                    if (!handled && locationUrl.indexOf(url) != -1) {
                        var isException = false;
                        $.each(_ajaxifiedUrlOptions[action].urlExceptions, function (urlExceptionIndex, urlException) {
                            if (!isException && locationUrl.indexOf(urlException) != -1) {
                                isException = true;
                            }
                        });
                        if (!isException) {
                            handled = true;
                            locationAction = action;
                        }
                    }
                });
                if (handled) {
                    break;
                }
            }
            _urlAjaxActions[locationUrl] = locationAction;
        }
        return _urlAjaxActions[locationUrl];
    }

    function _processAjaxifiedUrl(url) {
        var action = _urlAjaxAction(url);
        if (action) {
            var selectors = $.options('#m-ajax').selectors[action];
            _ajaxifiedUrlOptions[action].callback(url, _lastAjaxActionSource, action, selectors);
            return false; // prevent default link click behavior
        }
        return true;
    }

    function setLocation(locationUrl, element) {
        var action = _urlAjaxAction(locationUrl);
        if (action) {
            _lastAjaxActionSource = element;
            locationUrl = window.decodeURIComponent(locationUrl);
            if (window.History && window.History.enabled) {
                window.History.pushState(null, window.title, locationUrl);
            }
            else {
                _processAjaxifiedUrl(locationUrl);
            }
        }
        else {
            oldSetLocation(locationUrl, element);
        }
        return false;
    }

    // the following function is executed when DOM ir ready. If not use this wrapper, code inside could fail if
    // executed when referenced DOM elements are still being loaded.
    $(function () {
        if (window.History && window.History.enabled) {
            $(window).bind('statechange', function () {
                var State = window.History.getState();
                _processAjaxifiedUrl(State.url);
            });
        }

        if (window.setLocation) {
            oldSetLocation = window.setLocation;
            window.setLocation = setLocation;
        }



	$(document).on('click', 'a', function() {
            var action = _urlAjaxAction(this.href);
            if (action) {
                return setLocation(this.href, this);
            }
        });


	$(document).on('click', '.m-filter-item-list li input', function() {
            var parentElement = $(this).closest('form');
            var queryString = "";

            var totalList = $("input:checked", parentElement).length;

            $("input:checked", parentElement).each(function(i) {
                var k = i+1;
                if( k == totalList){
                    queryString += $(this).val();  
                }else{
                    queryString += $(this).val() + "_";  
                }
            });

            if(!queryString == ""){
                var re = new RegExp( '&' + $(this).attr('name') + '=[^&;]*' , '');
                var sendUrl = $("a.send-button", parentElement).attr('href').replace(re,'');
                if(sendUrl){
                    queryString = "&" + $(this).attr('name') + "=" + queryString;
                }
                if(sendUrl.indexOf('?') == -1){
                    sendUrl += "?" + queryString;
                }else{
                    sendUrl += queryString;
                }
                $("a.send-button", parentElement).attr('href', sendUrl);
                $("a.send-button", parentElement).parent().fadeIn('slow');
            }else{
                $("a.send-button", parentElement).parent().fadeOut();
            }

        });


        // custom css expression for a case-insensitive contains()
        $.expr[':'].Contains = function(a,i,m){
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
        };


	$(document).on('change', 'form.scrollbox .header-input input.filterinput', function() {
                var filter = $(this).val();
                var list = $(this).closest('form.scrollbox').children('ol.scrollbox');
                if(filter) {
                    // this finds all links in a list that contain the input,
                    // and hide the ones not containing the input while showing the ones that do
                    $(list).find("label:not(:Contains(" + filter + "))").parent().slideUp();
                    $(list).find("label:Contains(" + filter + ")").parent().slideDown();
                } else {
                    $(list).find("li").slideDown();
                }
                return false;
            })
	    .on('keyup', 'form.scrollbox .header-input input.filterinput', function() {
                // fire the above change event after every letter
                $(this).change();
            });     

    });

    //endregion

    //region AJAX content get/update API

    $.mGetBlocksHtml = function(url, action, selectors, callback) {
        $(document).trigger('m-ajax-before', [selectors, url, action]);
        $.get(window.encodeURI(url + (url.indexOf('?') == -1 ? '?' : '&') + 'm-ajax=' + action + '&no_cache=1'))
            .done(function (response) {
                try {
                    response = $.parseJSON(response);
                    if (!response) {
                        if ($.options('#m-ajax').debug) {
                            alert('No response.');
                        }
                    }
                    else if (response.error) {
                        if ($.options('#m-ajax').debug) {
                            alert(response.error);
                        }
                    }
                    else {
                        callback(response, selectors, action, url);
                    }
                }
                catch (error) {
                    if ($.options('#m-ajax').debug) {
                        alert(response && typeof(response) == 'string' ? response : error);
                    }
                }
            })
            .fail(function (error) {
                if ($.options('#m-ajax').debug) {
                    alert(error.status + (error.responseText ? ': ' + error.responseText : ''));
                }
            })
            .complete(function () {
                $(document).trigger('m-ajax-after', [selectors, url, action]);
            });
    }
    $.mUpdateBlocksHtml = function(response) {
        $.dynamicReplace(response.update, $.options('#m-ajax').debug, true);
        if (response.options) {
            $.options(response.options);
        }
        if (response.script) {
            $.globalEval(response.script);
        }
        if (response.title) {
            document.title = response.title;
        }
    };

    $.mGetBlockHtml = function (url, action, callback) {
        $(document).trigger('m-ajax-before', [[], url, action]);
        $.get(window.encodeURI(url))
            .done(function (response) {
                try {
                    if (!response) {
                        if ($.options('#m-ajax').debug) {
                            alert('No response.');
                        }
                    }
                    else if (response.isJSON()) {
                        response = $.parseJSON(response);
                        if (response.error) {
                            if ($.options('#m-ajax').debug) {
                                alert(response.error);
                            }
                        }
                    }
                    else {
                        callback(response, url);
                    }
                }
                catch (error) {
                    if ($.options('#m-ajax').debug) {
                        alert(response && typeof(response) == 'string' ? response : error);
                    }
                }
            })
            .fail(function (error) {
                if ($.options('#m-ajax').debug) {
                    alert(error.status + (error.responseText ? ': ' + error.responseText : ''));
                }
            })
            .complete(function () {
                $(document).trigger('m-ajax-after', [[], url, action]);
            });
    }
    //endregion

    //region default progress visualization
    $(document).bind('m-ajax-before', function (e, selectors, url, action) {
        if ($.options('#m-ajax').overlay[action]) {
            var loaderImgSrc = $('#m-wait p#loading_mask_loader img').attr('src');
            var loadImage = "";
            if (loaderImgSrc) {
                loadImage = "<img alt='Loading...' src='" + loaderImgSrc +"'>";
            };
            if (selectors.length) {
                selectors.each(function (selector, selectorIndex) {

                    var vars = [], hash, anchor;
                    var q = url.split('?')[1];
                    if(q != undefined){
                        q = q.split('&');
                        for(var i = 0; i < q.length; i++){
                            if(!q[i] === ''){
                                hash = q[i].split('=');
                                anchor = hash[1].split('#');
                                vars.push(anchor[0]);
                                vars[hash[0]] = anchor[0]; 
                            }
                        }
                    }

                    if( (selector == ".mb-category-products") && (typeof vars['attr_code'] != 'undefined') ){
                        if(vars['attr_code'] == ''){
                            var left = 0, top = 0, right = 0, bottom = 0, assigned = false;
                            $(selector).each(function () {
                                var element = $(this);
                                var elOffset = element.offset(), elWidth = element.width(), elHeight = element.height();
                                if (!assigned || left > elOffset.left) {
                                    left = elOffset.left;
                                }
                                if (!assigned || top > elOffset.top) {
                                    top = elOffset.top;
                                }
                                if (!assigned || right < elOffset.left + elWidth) {
                                    right = elOffset.left + elWidth;
                                }
                                if (!assigned || bottom < elOffset.top + elHeight) {
                                    bottom = elOffset.top + elHeight;
                                }
                                assigned = true;
                            });
                            if (assigned) {
                                // create overlay
                                var overlay = $('<div class="m-overlay"><div class="loader">'+ loadImage +'<br /><p>Please wait...</p></div></div>');
                                overlay.appendTo(document.body);
                                overlay.css({left:left, top:top}).width(right - left).height(bottom - top);
                                overlay.children('.loader').width(right - left);
                            }
                        }

                    }else{
                        var left = 0, top = 0, right = 0, bottom = 0, assigned = false;
                        $(selector).each(function () {
                            var element = $(this);
                            var elOffset = element.offset(), elWidth = element.width(), elHeight = element.height();
                            if (!assigned || left > elOffset.left) {
                                left = elOffset.left;
                            }
                            if (!assigned || top > elOffset.top) {
                                top = elOffset.top;
                            }
                            if (!assigned || right < elOffset.left + elWidth) {
                                right = elOffset.left + elWidth;
                            }
                            if (!assigned || bottom < elOffset.top + elHeight) {
                                bottom = elOffset.top + elHeight;
                            }
                            assigned = true;
                        });
                        if (assigned) {
                            // create overlay
                            var overlay = $('<div class="m-overlay"><div class="loader">'+ loadImage +'<br /><p>Please wait...</p></div></div>');
                            overlay.appendTo(document.body);
                            overlay.css({left:left, top:top}).width(right - left).height(bottom - top);
                            overlay.children('.loader').width(right - left);
                        }
                    }
                });
            }
            else {
                var overlay = $('<div class="m-overlay"><div class="loader">'+ loadImage +'<br /><p>Please wait...</p></div></div>');
                overlay.appendTo(document.body);
                overlay.css({left:0, top:0}).width($(document).width()).height($(document).height());
                overlay.children('.loader').width($(document).width());
            }
        }

        if ($.options('#m-ajax').progress[action]) {
            $('#m-popup').show();
            $('#m-wait').show();
        }
    });
    $(document).bind('m-ajax-after', function (e, selectors, url, action) {
        // remove overlays
        if ($.options('#m-ajax').overlay[action]) {
            $('.m-overlay').remove();
        }
        if ($.options('#m-ajax').progress[action]) {
            $('#m-popup').hide();
            $('#m-wait').hide();
        }
    });
    //endregion

    $(document).bind('m-ajax-after', function (e, selectors, url, action) {
        // remove overlays
        if ($.options('#m-ajax').overlay[action]) {
            $('.m-overlay').remove();
        }
        if ($.options('#m-ajax').progress[action]) {
            $('#m-popup').hide();
            $('#m-wait').hide();
        }
    });

})(jQuery);
// fix for rwd theme

(function($) {
    var openFilters = [];
    $(document).ready(function () {
    });
    $(document).bind('m-ajax-before', function (e, selectors, url, action) {
        openFilters = [];
        $('.block-layered-nav .toggle-content').children('dl:first').children('dt.current').each(function () {
            openFilters.push($(this).data('id'));
        });
    });
    $(document).bind('m-ajax-after', function (e, selectors, url, action) {
        $('.block-layered-nav .toggle-content').each(function () {
            var wrapper = jQuery(this);

            var hasTabs = wrapper.hasClass('tabs');
            var hasAccordion = wrapper.hasClass('accordion');
            var startOpen = wrapper.hasClass('open');

            var dl = wrapper.children('dl');
            var dts = dl.children('dt');
            var panes = dl.children('dd');
            var groups = new Array(dts, panes);

            //Create a ul for tabs if necessary.
            if (hasTabs) {
                var ul = jQuery('<ul class="toggle-tabs"></ul>');
                dts.each(function () {
                    var dt = jQuery(this);
                    var li = jQuery('<li></li>');
                    li.html(dt.html());
                    ul.append(li);
                });
                ul.insertBefore(dl);
                var lis = ul.children();
                groups.push(lis);
            }

            //Add "last" classes.
            var i;
            for (i = 0; i < groups.length; i++) {
                groups[i].filter(':last').addClass('last');
            }

            function toggleClasses(clickedItem, group) {
                var index = group.index(clickedItem);
                var i;
                for (i = 0; i < groups.length; i++) {
                    groups[i].removeClass('current');
                    groups[i].eq(index).addClass('current');
                }
            }

            //Toggle on tab (dt) click.
            dts.on('click', function (e) {
                //They clicked the current dt to close it. Restore the wrapper to unclicked state.
                if (jQuery(this).hasClass('current') && wrapper.hasClass('accordion-open')) {
                    wrapper.removeClass('accordion-open');
                } else {
                    //They're clicking something new. Reflect the explicit user interaction.
                    wrapper.addClass('accordion-open');
                }
                toggleClasses(jQuery(this), dts);
            });

            //Toggle on tab (li) click.
            if (hasTabs) {
                lis.on('click', function (e) {
                    toggleClasses(jQuery(this), lis);
                });
                //Open the first tab.
                lis.eq(0).trigger('click');
            }

        });
        $.each(openFilters, function(index, id) {
            $('dt[data-id="' + id + '"]').trigger('click');
        });
        openFilters = [];

        $(window).trigger('delayed-resize');
    });

})(jQuery);
