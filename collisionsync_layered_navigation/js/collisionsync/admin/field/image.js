(function($) {
    function _buttons(tr) {
        if ($(tr).find('.m-default').attr('checked') == 'checked') {
            $(tr).find('.mfi-buttons .m-button').hide();
        }
        else {
            $(tr).find('.mfi-buttons .m-button').show();
            if ($(tr).find('div.field-image').prev().val() == '') {
                $(tr).find('.mfi-buttons .change.m-button, .mfi-buttons .delete.m-button').hide();
            }
            else {
                $(tr).find('.mfi-buttons .add.m-button').hide();
            }
        }
    }
    $(function() {
        $(document).bind('m-image-field-reset', function(e, tr) {
            // file uploader initialization
            // the following shows the button in specified element with file upload behavior on click
            var addUploader = new qq.FileUploader({
                // pass the dom node (ex. $(selector)[0] for jQuery users)
                element: $(tr).find('.add.m-button')[0],
                // path to server-side upload script
                action: $.options("#m-image-helper").uploadUrl,
                params: { type: 'image', form_key: FORM_KEY },
                onSubmit: function(id, fileName) {
                    addUploader._options.params.id = tr.id;
                },
                // when upload complete we should update image in grid
                onComplete: function(id, fileName, responseJSON){
                    if (responseJSON.id) {
                        var baseUrl = $.options('#m-image-helper').baseUrl + '/';
                        $('#'+responseJSON.id).find('div.field-image').prev().val(responseJSON.relativeUrl);
                        $('#'+responseJSON.id).find('div.field-image').css({'background-image': 'url(' + responseJSON.url + ')'});
                        _buttons(tr);
                        $(tr).find('.add.m-button').val('');
                    }
                }
            });
            var changeUploader = new qq.FileUploader({
                // pass the dom node (ex. $(selector)[0] for jQuery users)
                element: $(tr).find('.change.m-button')[0],
                // path to server-side upload script
                action: $.options("#m-image-helper").uploadUrl,
                params: { type: 'image', form_key: FORM_KEY },
                onSubmit: function(id, fileName) {
                    changeUploader._options.params.id = tr.id;
                },
                // when upload complete we should update image in grid
                onComplete: function(id, fileName, responseJSON){
                    if (responseJSON.id) {
                        var baseUrl = $.options('#m-image-helper').baseUrl + '/';
                        $('#'+responseJSON.id).find('div.field-image').prev().val(responseJSON.relativeUrl);
                        $('#'+responseJSON.id).find('div.field-image').css({'background-image': 'url(' + responseJSON.url + ')'});
                        _buttons(tr);
                        $(tr).find('.change.m-button').val('');
                    }
                }
            });
            $(tr).find('.delete.m-button').click(function() {
                $(tr).find('div.field-image').prev().val('');
                $(tr).find('div.field-image').css({'background-image': ''});
                 _buttons(tr);
            });
            $(tr).find('.m-default').click(function() {
                 _buttons(tr);
            });
            _buttons(tr);
        });
    });
})(jQuery);
