(function($) {
	var _helper = null;
	
	function _onHelperShow(td, helper) {
		$(helper).find('input.m-default').mMarkAttr('checked', $.gridData(td, 'is_default'));
		$(helper).find('input.m-default').mMarkAttr('disabled', $.gridData(td, 'is_default_disabled'));
		_helper = helper;
		// show/hide here in more complex helpers
	}
	function _onHelperHide(td, helper) {
		$.gridData(td, {is_default: $(helper).find('input.m-default').attr('checked') == 'checked'});
		_helper = null;
	}
	// the following function is executed when DOM ir ready. If not use this wrapper, code inside could fail if
	// executed when referenced DOM elements are still being loaded.
	$(function() {
		$(document).on('mouseover', '.ct-input', function() {
			if ($.gridData(this, 'show_helper')) { 
				$.helperPopup({
					host: this, 
					helper: '#m-column-helper', 
					onShow: _onHelperShow,
					onHide: _onHelperHide
				});
			}
		});
		$(document).on('change', '.ct-input input', function() {
			var td = $(this).parent('td')[0];
			$.gridData(td, {value : $(this).val(), is_default: false});
			if (_helper) {
				$(_helper).find('input.m-default').mMarkAttr('checked', $.gridData(td, 'is_default'));
			}
		});
	});
})(jQuery);
