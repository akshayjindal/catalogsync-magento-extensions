<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */
/**
 * Generic helper functions for Collisionsync_Ajax module. This class is a must for any module even if empty.
 * @author Collisionsync Team
 */
class Collisionsync_Ajax_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getAllowedActions($actionName) {
        $actionNodes = Mage::helper('collisionsync_core')->getSortedXmlChildren(
            Mage::getConfig()->getNode('collisionsync_ajax/allowed_actions'), $actionName);
        if (count($actionNodes)) {
            $result = array();
            foreach ($actionNodes as $actionNode) {
                $result[] = $actionNode->getName();
            }
            return $result;
        }
        else {
            return false;
        }
    }
    protected $_detected = false;
    protected $_enabled = false;
    public function isEnabled() {
        if (!$this->_detected) {
            switch (Mage::getStoreConfig('collisionsync/ajax/mode')) {
                case Collisionsync_Ajax_Model_Mode::OFF:
                    break;
                case Collisionsync_Ajax_Model_Mode::ON_FOR_ALL:
                    $this->_enabled = true;
                    break;
                case Collisionsync_Ajax_Model_Mode::ON_FOR_USERS:
                    $this->_enabled = true;
                    foreach (explode(';', Mage::getStoreConfig('collisionsync/ajax/bots')) as $agent) {
                        if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], trim($agent)) !== false) {
                            $this->_enabled = false;
                            break;
                        }
                    }
                    break;
                default:
                    throw new Exception('Not implemented');
            }
            $this->_detected = true;
        }
        return $this->_enabled;
    }
}