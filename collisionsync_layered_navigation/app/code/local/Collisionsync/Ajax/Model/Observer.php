<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */
/**
 * This class observes certain (defined in etc/config.xml) events in the whole system and provides public methods -
 * handlers for these events.
 * @author Collisionsync Team
 *
 */
class Collisionsync_Ajax_Model_Observer {
    /**
     * If AJAX is enabled on page, raises global flag to wrap all AJAX-able blocks into container DIV elements.
     * If in addition, current request asks only for AJAX content, cancels full page rendering and instead
     * raises global flag to render only AJAX content (handles event "controller_action_predispatch")
     * @param Varien_Event_Observer $observer
     */
    public function ajaxifyPage($observer) {
        /* @var $controller Mage_Core_Controller_Varien_Action */ $controller = $observer->getEvent()->getControllerAction();
        $actionName = $controller->getFullActionName();
        if ($allowedAjaxActions = Mage::helper('collisionsync_ajax')->getAllowedActions($actionName)) {
            Mage::register('m_wrap_updatable_html_blocks', true);
            if (($ajaxAction = $controller->getRequest()->getParam('m-ajax')) && in_array($ajaxAction, $allowedAjaxActions)) {
                Mage::helper('collisionsync_core')->updateRequestParameter('m-ajax', '', $ajaxAction);
                Mage::helper('collisionsync_core')->updateRequestParameter('no_cache', '', '1');
                Mage::register('m_current_ajax_action', $ajaxAction);

                Mage::dispatchEvent('m_ajax_request');
                Mage::app()->getFrontController()->setNoRender(true);
            }
        }
    }

    /**
     * If relevant global flag is raised, wrap all AJAX-able blocks into container DIV elements
     * (handles event "core_block_abstract_to_html_after")
     * @param Varien_Event_Observer $observer
     */
    public function wrapUpdatableHtml($observer) {
        if (Mage::registry('m_wrap_updatable_html_blocks')) {
            /* @var $block Mage_Core_Block_Abstract */ $block = $observer->getEvent()->getBlock();
            /* @var $transport Varien_Object */ $transport = $observer->getEvent()->getTransport();

            if ($block->getLayout() && ($updateBlock = $block->getLayout()->getBlock('m_ajax_update'))) {
                $transport->setHtml($updateBlock->markUpdatable($block->getNameInLayout(), $transport->getHtml()));
            }
        }
    }

    /**
     * If relevant global flag is raised, renders AJAX content into JSON response instead of typical full-page
     * HTML response (handles event "controller_front_send_response_before")
     * @param Varien_Event_Observer $observer
     */
    public function renderAjaxResponse($observer) {
        if ($action = Mage::registry('m_current_ajax_action')) {
            $response = new Varien_Object();
            Mage::dispatchEvent('m_ajax_response', compact('action', 'response'));
            if ($response->getIsHandled()) {
                $response = $response->getData();
                unset($response['is_handled']);
            }
            else {
                $response = $this->getLayout()->getBlock('m_ajax_update')->toAjaxHtml($action);
            }
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    /**
     * Retrieve current layout object
     *
     * @return Mage_Core_Model_Layout
     */
    public function getLayout() {
        return Mage::getSingleton('core/layout');
    }
    /**
     * Retrieve response object
     *
     * @return Mage_Core_Controller_Response_Http
     */
    public function getResponse() {
        return Mage::app()->getResponse();
    }


    /**
     * REPLACE THIS WITH DESCRIPTION (handles event "m_ajax_options")
     * @param Varien_Event_Observer $observer
     */
    public function renderOptions($observer) {
        $actionName = str_replace('/', '_', Mage::helper('collisionsync_core')->getRoutePath());
        if ($config = Mage::getConfig()->getNode('collisionsync_ajax/urls/'.$actionName)) {
            $class = (string)$config->class;
            $method = (string)$config->method;
            $obj = new $class;
            $options = $obj->$method();

            $generalUrlExceptions = array();
            foreach (Mage::app()->getStores() as $store) {
                /* @var $store Mage_Core_Model_Store */
                if ($store->getId() != Mage::app()->getStore()->getId()) {
                    $generalUrlExceptions[$store->getCurrentUrl()] = $store->getCurrentUrl();
                }
            }

            $options = array_merge(array(
                'scroll' => Mage::getStoreConfigFlag('collisionsync/ajax/scroll_to_top_filter'),
            ), $options);
            $options['urlExceptions'] = isset($options['urlExceptions'])
                ? array_merge($generalUrlExceptions, $options['urlExceptions'])
                : $generalUrlExceptions;
            Mage::helper('collisionsync_core/js')->options('#m-filter-ajax', $options);
        }
    }

    public function getCategoryOptions() {
        $exactUrls = array();
        $partialUrls = array();
        $urlExceptions = array();

        /* @var $category Mage_Catalog_Model_Category */
        $category = Mage::registry('current_category');
        /* @var $core Collisionsync_Core_Helper_Data */
        $core = Mage::helper(strtolower('Collisionsync_Core'));

        Collisionsync_Core_Profiler::start('mln', __CLASS__, __METHOD__, '$category->getUrl()');
        $url = Mage::helper('collisionsync_filters')->getClearUrl(false, true); //$category->getUrl();
        if (($pos = mb_strrpos($url, '?')) !== false) {
            $url = mb_substr($url, 0, $pos);
        }
        Collisionsync_Core_Profiler::stop('mln', __CLASS__, __METHOD__, '$category->getUrl()');
        //if ($core->endsWith($url, '/')) {
        //  $url = substr($url, 0, strlen($url) - 1);
        //}

        $exactUrls[$url] = $url;
        $partialUrls[$url . '?'] = $url . '?';
        if ($categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix()) {
            if (($pos = mb_strrpos($url, $categorySuffix)) !== false) {
                if ($pos + mb_strlen($categorySuffix) < mb_strlen($url)) {
                    $url = mb_substr($url, 0, $pos) . mb_substr($url, $pos + mb_strlen($categorySuffix));
                } else {
                    $url = mb_substr($url, 0, $pos);
                }
            }
            if ($conditionalWord = $core->getStoreConfig('collisionsync_filters/seo/conditional_word')) {
                $partialUrls[$url . '/' . $conditionalWord] = $url . '/' . $conditionalWord;
            }
        } else {
            if ($conditionalWord = $core->getStoreConfig('collisionsync_filters/seo/conditional_word')) {
                $partialUrls[$url . '/' . $conditionalWord] = $url . '/' . $conditionalWord;
            }
        }

        Collisionsync_Core_Profiler::start('mln', __CLASS__, __METHOD__, '$category->getChildrenCategories()');
        $childCategories = $category->getChildrenCategories();
        Collisionsync_Core_Profiler::stop('mln', __CLASS__, __METHOD__, '$category->getChildrenCategories()');
        foreach ($childCategories as $childCategory) {
            $url = $childCategory->getUrl();
            if (Mage::app()->getFrontController()->getRequest()->isSecure()) {
                $url = str_replace('http://', 'https://', $url);
            }
            if ($core->endsWith($url, '/')) {
                $url = substr($url, 0, strlen($url) - 1);
            }
            if ($categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix()) {
                $url = str_replace($categorySuffix, '', $url);
            }
            $urlExceptions[$url] = $url;
        }

        return compact('exactUrls', 'partialUrls', 'urlExceptions');
    }

    public function getSearchOptions() {
        $exactUrls = array();
        $partialUrls = array();
        $urlExceptions = array();

        /* @var $category Mage_Catalog_Model_Category */
        $category = Mage::registry('current_category');
        /* @var $core Collisionsync_Core_Helper_Data */
        $core = Mage::helper(strtolower('Collisionsync_Core'));
        $request = Mage::app()->getRequest();

        $url = Mage::getSingleton('core/url')->sessionUrlVar(Mage::helper('core')->escapeUrl(Mage::getUrl()));
        if ($core->endsWith($url, '/')) {
            $url = substr($url, 0, strlen($url) - 1);
        }
        $url .= $request->getOriginalPathInfo();
        $partialUrls[$url] = $url;

        return compact('exactUrls', 'partialUrls', 'urlExceptions');
    }

    public function getPageOptions() {
        $exactUrls = array();
        $partialUrls = array();
        $urlExceptions = array();

        /* @var $core Collisionsync_Core_Helper_Data */
        $core = Mage::helper(strtolower('Collisionsync_Core'));
        $request = Mage::app()->getRequest();

        $url = Mage::helper('cms/page')->getPageUrl($request->getParam('page_id'));
        $exactUrls[$url] = $url;
        $partialUrls[$url . '?'] = $url . '?';
        if ($conditionalWord = $core->getStoreConfig('collisionsync_filters/seo/conditional_word')) {
            $partialUrls[$url . '/' . $conditionalWord] = $url . '/' . $conditionalWord;
        }

        return compact('exactUrls', 'partialUrls', 'urlExceptions');
    }

    public function getHomeOptions() {

        $exactUrls = array();
        $partialUrls = array();
        $urlExceptions = array();

        /* @var $core Collisionsync_Core_Helper_Data */
        $core = Mage::helper(strtolower('Collisionsync_Core'));

        $url = Mage::getUrl();
        $exactUrls[$url] = $url;
        if ($core->endsWith($url, '___SID=U')) {
            $url = substr($url, 0, strlen($url) - strlen('___SID=U'));
        }
        if ($core->endsWith($url, '?')) {
            $url = substr($url, 0, strlen($url) - 1);
        }
        if ($core->endsWith($url, '/')) {
            $url = substr($url, 0, strlen($url) - 1);
        }
        $partialUrls[$url . '?'] = $url . '?';
        if ($conditionalWord = $core->getStoreConfig('collisionsync_filters/seo/conditional_word')) {
            $partialUrls[$url . '/' . $conditionalWord] = $url . '/' . $conditionalWord;
        }

        return compact('exactUrls', 'partialUrls', 'urlExceptions');
    }

    /* obsolete handlers. Kept here for easier upgrade */
    public function registerUrl($observer) { }
    public function ajaxCategoryView($observer) {}
    public function ajaxSearchResult($observer) {}
    public function markUpdatableHtml($observer) {}
    public function ajaxCmsIndex($observer) {}
    public function ajaxCmsPage($observer) {}
}
