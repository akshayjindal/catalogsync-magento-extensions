<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

class Collisionsync_Ajax_Model_Source_Mode extends Collisionsync_Core_Model_Source_Abstract {
	protected function _getAllOptions() {
		return array(
            array('value' => Collisionsync_Ajax_Model_Mode::OFF, 'label' => Mage::helper('collisionsync_ajax')->__('No')),
            array('value' => Collisionsync_Ajax_Model_Mode::ON_FOR_ALL, 'label' => Mage::helper('collisionsync_ajax')->__('Yes')),
            array('value' => Collisionsync_Ajax_Model_Mode::ON_FOR_USERS, 'label' => Mage::helper('collisionsync_ajax')->__('Yes for Users, No for Search Bots (Listed Below)')),
        );
	}
}