<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

class Collisionsync_Ajax_Model_Source_Method extends Collisionsync_Core_Model_Source_Abstract {
	protected function _getAllOptions() {
		return array(
            array('value' => Collisionsync_Ajax_Model_Method::MARK_WITH_CSS_CLASS, 'label' => Mage::helper('collisionsync_ajax')->__('Mark with CSS class')),
            array('value' => Collisionsync_Ajax_Model_Method::WRAP_INTO_CONTAINER, 'label' => Mage::helper('collisionsync_ajax')->__('Wrap into HTML element')),
        );
	}
}