<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

class Collisionsync_Ajax_Model_Method {
	const MARK_WITH_CSS_CLASS = 1;
	const WRAP_INTO_CONTAINER = 2;
}