<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Ajax
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

class Collisionsync_Ajax_Model_Mode {
	const OFF = 0;
	const ON_FOR_ALL = 1;
	const ON_FOR_USERS = 2;
}