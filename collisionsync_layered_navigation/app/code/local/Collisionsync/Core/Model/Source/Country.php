<?php
/** 
 * @category    Collisionsync
 * @package     Collisionsync_Core
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * @author Collisionsync Team
 *
 */
class Collisionsync_Core_Model_Source_Country extends Collisionsync_Core_Model_Source_Abstract {
    protected function _getAllOptions() {
        return Mage::getResourceModel('directory/country_collection')->load()->toOptionArray();
    }
}