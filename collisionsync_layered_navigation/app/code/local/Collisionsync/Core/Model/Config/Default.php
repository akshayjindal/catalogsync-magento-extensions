<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Core
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Default value provider which gets value from global configuration
 * @author Collisionsync Team
 *
 */
class Collisionsync_Core_Model_Config_Default {
	public function getDefaultValue($model, $attributeCode, $path) {
		return Mage::getStoreConfig($path);
	}
	public function getUseDefaultLabel() {
		return Mage::helper('collisionsync_core')->__('Use System Configuration');
	}
}