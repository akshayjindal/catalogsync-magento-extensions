<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Db
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 * PROPERTY METHODS
 * 
 * @method string getEntityName()
 * @method bool hasEntityName(()
 * @method Collisionsync_Db_Model_Replication_Target unsEntityName(()
 * @method Collisionsync_Db_Model_Replication_Target setEntityName(()
 * 
 * @method bool getIsKeyFilterApplied()
 * @method bool hasIsKeyFilterApplied(()
 * @method Collisionsync_Db_Model_Replication_Target unsIsKeyFilterApplied(()
 * @method Collisionsync_Db_Model_Replication_Target setIsKeyFilterApplied(()
 * 
 * @method bool getReplicable()
 * @method bool hasReplicable()
 * @method Collisionsync_Db_Replication_Target unsReplicable()
 * @method Collisionsync_Db_Replication_Target setReplicable()
 * 
 * COLLECTION METHODS
 * 
 * @method array getSourceEntityNames()
 * @method string getSourceEntityName()
 * @method bool hasSourceEntityName()
 * @method Collisionsync_Db_Model_Replication_Target setSourceEntityName()
 * @method Collisionsync_Db_Model_Replication_Target setSourceEntityNames()
 * @method Collisionsync_Db_Model_Replication_Target unsSourceEntityName()
 * 
 * @method array getSavedKeys()
 * @method string getSavedKey()
 * @method bool hasSavedKey()
 * @method Collisionsync_Db_Model_Replication_Target setSavedKey()
 * @method Collisionsync_Db_Model_Replication_Target setSavedKeys()
 * @method Collisionsync_Db_Model_Replication_Target unsSavedKey()
 * 
 * @method array getDeletedKeys()
 * @method string getDeletedKey()
 * @method bool hasDeletedKey()
 * @method Collisionsync_Db_Model_Replication_Target setDeletedKey()
 * @method Collisionsync_Db_Model_Replication_Target setDeletedKeys()
 * @method Collisionsync_Db_Model_Replication_Target unsDeletedKey()
 * 
 * @method array getSelects()
 * @method Varien_Db_Select getSelect()
 * @method bool hasSelect()
 * @method Collisionsync_Db_Model_Replication_Target setSelect()
 * @method Collisionsync_Db_Model_Replication_Target setSelects()
 * @method Collisionsync_Db_Model_Replication_Target unsSelect()
 * 
 * 
 */
class Collisionsync_Db_Model_Replication_Target extends Collisionsync_Core_Model_Object {
	protected $_collections = array(
		'source_entity_names' => array(),
		'saved_keys' => array(),
		'deleted_keys' => array(),
		'selects' => array(),
	);
}