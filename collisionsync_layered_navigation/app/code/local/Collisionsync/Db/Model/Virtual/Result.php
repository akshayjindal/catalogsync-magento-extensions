<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Db
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 * ARRAY METHODS
 * 
 * @method array getColumns()
 * @method string getColumn()
 * @method Collisionsync_Db_Model_Virtual_Result unsColumns()
 * @method Collisionsync_Db_Model_Virtual_Result unsColumn()
 * @method Collisionsync_Db_Model_Virtual_Result addColumn()
 * @method Collisionsync_Db_Model_Virtual_Result setColumns()
 * 
 */
class Collisionsync_Db_Model_Virtual_Result extends Collisionsync_Core_Model_Object {
	protected $_arrays = array(
		'columns' => array(),
	);
}