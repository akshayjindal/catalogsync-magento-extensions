<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Db
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

/**
 * Entry points for cron and index processes
 * @author Collisionsync Team
 *
 */
class Collisionsync_Db_Model_Indexer extends Mage_Index_Model_Indexer_Abstract {
	// INDEXING ITSELF
	
    protected function _construct()
    {
        $this->_init('collisionsync_db/replicate');
    }
    public function getName()
    {
        return Mage::helper('collisionsync_db')->__('Default Values');
    }
    public function getDescription()
    {
        return Mage::helper('collisionsync_db')->__('Propagate default values throughout the system');
    }
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
    }
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
    }
	public function reindexAll() {
		Mage::helper('collisionsync_db')->replicate();
	}
    
    public function runCronjob()
    {
        $this->reindexAll();
    }
}