<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Db
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 * ARRAY METHODS
 * 
 * @method array getErrors()
 * @method string getError()
 * @method Collisionsync_Db_Model_Validation unsErrors()
 * @method Collisionsync_Db_Model_Validation unsError()
 * @method Collisionsync_Db_Model_Validation addError()
 * @method Collisionsync_Db_Model_Validation setErrors()
 * 
 */
class Collisionsync_Db_Model_Validation extends Collisionsync_Core_Model_Object {
	protected $_arrays = array(
		'errors' => array(),
	);
} 