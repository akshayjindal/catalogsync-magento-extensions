<?php

class Collisionsync_FilterAdmin_Block_List_Container extends Collisionsync_Admin_Block_Crud_List_Container {
    public function __construct() {
        parent::__construct();
        $this->_headerText = $this->__('Layered Navigation Filters');
    }
}