<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/* BASED ON SNIPPET: New Module/Helper/Data.php */
/**
 * Generic helper functions for Collisionsync_Filters module. This class is a must for any module even if empty.
 * @author Collisionsync Team
 */
class Collisionsync_Filters_Helper_Data extends Mage_Core_Helper_Abstract {
	/**
	 * Recognizes block type based on its class. 
	 * OO purists would say that kind of ifs should be done using virtual functions. Here we ignore OO-ness and 
	 * micro performance penalty for the sake of clarity and keeping logic in one file.
	 * @param Mage_Catalog_Block_Layer_Filter_Abstract $block
	 * @return string
	 */
	public function getBlockType($block) {
		if ($block instanceof Collisionsync_Filters_Block_Filter_Attribute) return 'attribute';
		elseif ($block instanceof Collisionsync_Filters_Block_Filter_Category) return 'category';
		elseif ($block instanceof Collisionsync_Filters_Block_Filter_Decimal) return 'decimal';
		elseif ($block instanceof Collisionsync_Filters_Block_Filter_Price) return 'price';
		else throw new Exception('Not implemented');
	}
	/**
	 * Return unique filter name. 
	 * OO purists would say that kind of ifs should be done using virtual functions. Here we ignore OO-ness and 
	 * micro performance penalty for the sake of clarity and keeping logic in one file.
	 * @param Mage_Catalog_Model_Layer_Filter_Abstract $model
	 * @return string
	 */
	public function getFilterName($block, $model) {
		if ($model instanceof Collisionsync_Filters_Model_Filter_Category) {
            $result = 'category';
		}
		else {
            $result = $model->getAttributeModel()->getAttributeCode();
        }

        if ($showInFilter = $block->getShowInFilter()) {
            return $showInFilter . '_' . $result;
        } else {
            return $result;
        }
    }
	// INSERT HERE: helper functions that should be available from any other place in the system
	public function getJsPriceFormat() {
		return $this->formatPrice(0);
	}
	public function formatPrice($price) {
		$store = Mage::app()->getStore();
        if ($store->getCurrentCurrency()) {
            return $store->getCurrentCurrency()->formatPrecision($price, 0, array(), false, false);
        }
        return $price;
	}
	
	protected $_filterOptionsCollection;
    protected $_filterSearchOptionsCollection;
    protected $_filterAllOptionsCollection;
	public function getFilterOptionsCollection($allCategories = false) {
	    $request = Mage::app()->getRequest();
	    if ($request->getModuleName() == 'catalogsearch' && $request->getControllerName() == 'result' && $request->getActionName() == 'index' ||
	        $request->getModuleName() == 'collisionsyncpro_filterajax' && $request->getControllerName() == 'search' && $request->getActionName() == 'index')
	    {
            if (!$this->_filterSearchOptionsCollection) {
                $this->_filterSearchOptionsCollection = Mage::getResourceModel('collisionsync_filters/filter2_store_collection')
                        ->addColumnToSelect('*')
                        ->addStoreFilter(Mage::app()->getStore())
                        ->setOrder('position', 'ASC');
            }
            Mage::dispatchEvent('m_before_load_filter_collection', array('collection' => $this->_filterSearchOptionsCollection));
            return $this->_filterSearchOptionsCollection;
        }
		if ($allCategories) {
			if (!$this->_filterAllOptionsCollection) {
				$this->_filterAllOptionsCollection = Mage::getResourceModel('collisionsync_filters/filter2_store_collection')
		        	->addColumnToSelect('*')
		        	->addStoreFilter(Mage::app()->getStore())
		        	->setOrder('position', 'ASC');
			}
			Mage::dispatchEvent('m_before_load_filter_collection', array('collection' => $this->_filterAllOptionsCollection));
			return $this->_filterAllOptionsCollection;
		}
		else {
			if (!$this->_filterOptionsCollection) {
				Collisionsync_Core_Profiler::start('mln', __CLASS__, __METHOD__, '$productCollection->getSetIds()');
				$setIds = Mage::getSingleton('catalog/layer')->getProductCollection()->getSetIds();
				Collisionsync_Core_Profiler::stop('mln', __CLASS__, __METHOD__, '$productCollection->getSetIds()');
				$this->_filterOptionsCollection = Mage::getResourceModel('collisionsync_filters/filter2_store_collection')
		        	->addFieldToSelect('*')
		        	->addCodeFilter($this->_getAttributeCodes($setIds))
                    ->addStoreFilter(Mage::app()->getStore())
		        	->setOrder('position', 'ASC');
			}
            Mage::dispatchEvent('m_before_load_filter_collection', array('collection' => $this->_filterOptionsCollection));
            return $this->_filterOptionsCollection;
		}
	}
	protected function _getAttributeCodes($setIds) {
		/* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Collection */ 
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
		$collection->setAttributeSetFilter($setIds);
		$select = $collection->getSelect()
			->reset(Zend_Db_Select::COLUMNS)
			->columns('attribute_code');
		return array_merge($collection->getConnection()->fetchCol($select), array('category'));
	}
	public function markLayeredNavigationUrl($url, $routePath, $routeParams) {
	    $request = Mage::app()->getRequest();
	    $path = $request->getModuleName().'/'.$request->getControllerName(). '/'.$request->getActionName();
        if ($path == 'catalog/category/view') {
            if (Mage::getStoreConfigFlag('collisionsync_filters/session/save_applied_filters')) {
                $url .= (strpos($url, '?') === false) ? '?m-layered=1' : '&m-layered=1';
            }
        }
        elseif ($path == 'catalogsearch/result/index') {
            if (Mage::getStoreConfigFlag('collisionsync_filters/session/save_applied_search_filters')) {
                $url .= (strpos($url, '?') === false) ? '?m-layered=1' : '&m-layered=1';
            }
        }
        else {
            if (Mage::getStoreConfigFlag('collisionsync_filters/session/save_applied_cms_filters')) {
                $url .= (strpos($url, '?') === false) ? '?m-layered=1' : '&m-layered=1';
            }
        }
		return $url;
	}
    public function getClearUrl($markUrl = true, $clearListParams = false) {
        $filterState = array();
        foreach (array_merge(Mage::getSingleton('catalog/layer')->getState()->getFilters(), Mage::getSingleton('catalogsearch/layer')->getState()->getFilters()) as $item) {
            $filterState[$item->getFilter()->getRequestVar()] = $item->getFilter()->getCleanValue();
        }
        if ($clearListParams) {
            $filterState = array_merge($filterState, array(
              'dir' => null,
              'order' => null,
              'p' => null,
              'limit' => null,
              'mode' => null,
            ));
        }
        $params = array('_secure' => Mage::app()->getFrontController()->getRequest()->isSecure());
        $params['_current'] = true;
        $params['_use_rewrite'] = true;
        $params['_m_escape'] = '';
        $filterState['m-layered'] = null;
        $params['_query'] = $filterState;
        $result = Mage::getUrl('*/*/*', $params);
        if ($markUrl) {
            $result = $this->markLayeredNavigationUrl($result, '*/*/*', $params);
        }
        return $result;
    }
    public function getActiveFilters() {
        $filters = $this->getLayer()->getState()->getFilters();
        if (!is_array($filters)) {
            $filters = array();
        }
        return $filters;
    }
    public function resetProductCollectionWhereClause($select) {
        $preserved = new Varien_Object(array('preserved' => array()));
        $where = $select->getPart(Zend_Db_Select::WHERE);
        Mage::dispatchEvent('m_preserve_product_collection_where_clause', compact('where', 'preserved'));
        $preserved = $preserved->getPreserved();
        if (Mage::helper('collisionsync_core')->isMageVersionEqualOrGreater('1.7')) {
            foreach ($where as $key => $condition) {
                if (strpos($condition, 'e.website_id = ') !== false || strpos($condition, '`e`.`website_id` = ') !== false) {
                    $preserved[$key] = $key;
                }
                if (strpos($condition, 'e.customer_group_id = ') !== false || strpos($condition, '`e`.`customer_group_id` = ') !== false) {
                    $preserved[$key] = $key;
                }
            }

        }
        foreach ($where as $key => $condition) {
            if (!in_array($key, $preserved)) {
                unset($where[$key]);
            }
        }
        $where = array_values($where);
        if (isset($where[0]) && strpos($where[0], 'AND ') === 0) {
            $where[0] = substr($where[0], strlen('AND '));
        }
        $select->setPart(Zend_Db_Select::WHERE, $where);
    }
    public function getLayer () {
        if (in_array(Mage::helper('collisionsync_core')->getRoutePath(), array('catalogsearch/result/index', 'collisionsyncpro_filterajax/search/index'))) {
            return Mage::getSingleton('catalogsearch/layer');
        }
        else {
            return Mage::getSingleton('catalog/layer');
        }
    }
    public function canShowFilterInBlock($block, $filter) {
        if ($block->getData('show_'.$filter->getCode())) {
            return true;
        }
        elseif ($block->getData('hide_' . $filter->getCode())) {
            return false;
        }
        elseif ($showInFilter = $block->getShowInFilter()) {
            $showIn = $filter->getShowIn();
            if (!is_array($showIn)) {
                $showIn = explode(',', $showIn);
            }
            return in_array($showInFilter, $showIn);
        }
        else {
            return true;
        }
    }
    public function getFilterLayoutName($block, $filter) {
        if ($showInFilter = $block->getShowInFilter()) {
            return 'm_' . $showInFilter . '_' . $filter->getCode() . '_filter';
        }
        else {
            return 'm_' . $filter->getCode() . '_filter';
        }
    }

    public function addCountToCategories($productCollection, $categoryCollection) {
        $isAnchor = array();
        $isNotAnchor = array();
        foreach ($categoryCollection as $category) {
            if ($category->getIsAnchor()) {
                $isAnchor[] = $category->getId();
            } else {
                $isNotAnchor[] = $category->getId();
            }
        }
        $productCounts = array();
        if ($isAnchor || $isNotAnchor) {
            $select = $productCollection->getProductCountSelect();

            Mage::dispatchEvent(
                'catalog_product_collection_before_add_count_to_categories',
                array('collection' => $productCollection)
            );

            if ($isAnchor) {
                $anchorStmt = clone $select;
                $anchorStmt->limit(); //reset limits
                $anchorStmt->where('count_table.category_id IN (?)', $isAnchor);
                $productCounts += $productCollection->getConnection()->fetchPairs($anchorStmt);
                $anchorStmt = null;
            }
            if ($isNotAnchor) {
                $notAnchorStmt = clone $select;
                $notAnchorStmt->limit(); //reset limits
                $notAnchorStmt->where('count_table.category_id IN (?)', $isNotAnchor);
                $notAnchorStmt->where('count_table.is_parent = 1');
                $productCounts += $productCollection->getConnection()->fetchPairs($notAnchorStmt);
                $notAnchorStmt = null;
            }
            $select = null;
            $productCollection->unsProductCountSelect();
        }

        foreach ($categoryCollection as $category) {
            $_count = 0;
            if (isset($productCounts[$category->getId()])) {
                $_count = $productCounts[$category->getId()];
            }
            $category->setProductCount($_count);
        }

        return $this;
    }
}