<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Attribute definition collection for filters
 * @author Collisionsync Team
 *
 */
class Collisionsync_Filters_Resource_Filter_Attribute_Collection extends Collisionsync_Core_Resource_Attribute_Collection {
	public function __construct($resource=null) {
		$this->setEntityType(Collisionsync_Filters_Model_Filter::ENTITY);
		parent::__construct($resource);
	}
}