<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Block type for showing filters in category view pages.
 * @author Collisionsync Team
 * Injected into layout instead of standard catalog/layer_view in layout XML file.
 */
class Collisionsync_Filters_Block_View extends Mage_CatalogSearch_Block_Layer {
    
    /**
     * This method is called during page rendering to generate additional child blocks for this block.
     * @return Collisionsync_Filters_Block_View_Category
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     * @see app/code/core/Mage/Catalog/Block/Layer/Mage_Catalog_Block_Layer_View::_prepareLayout()
     */
    protected function _prepareLayout()
    {
        Mage::helper('collisionsync_core/layout')->delayPrepareLayout($this);

        return $this;
    }

    public function delayedPrepareLayout() {
        $showState = 'all';
        if ($showInFilter = $this->getShowInFilter()) {
            if ($template = Mage::getStoreConfig('collisionsync_filters/positioning/' . $showInFilter)) {
                $this->setTemplate($template);
            }
            $showState = Mage::getStoreConfig('collisionsync_filters/positioning/show_state_' . $showInFilter);
        }
        if ($showState) {
            $stateBlock = $this->getLayout()->createBlock('collisionsync_filters/state')
                    ->setLayer($this->getLayer())
                    ->setMode($showState);
            $this->setChild('layer_state', $stateBlock);
        }

        if (Mage::app()->getFrontController()->getRequest()->getModuleName() == 'catalogsearch') $_mode = 'search';
        else $_mode = 'category';
                    
        foreach (Mage::helper('collisionsync_filters')->getFilterOptionsCollection() as $filterOptions) {
            if (Mage::helper('collisionsync_filters')->canShowFilterInBlock($this, $filterOptions)) {
                $displayOptions = $filterOptions->getDisplayOptions();
                $block = $this->getLayout()->createBlock((string)$displayOptions->block, Mage::helper('collisionsync_filters')->getFilterLayoutName($this, $filterOptions), array(
                    'filter_options' => $filterOptions,
                    'display_options' => $displayOptions,
                    'show_in_filter' => $this->getShowInFilter(),
                ))->setLayer($this->getLayer());
                if ($attribute = $filterOptions->getAttribute()) {
                    $block->setAttributeModel($attribute);
                }
                $block->setMode($_mode)->init();
                $this->setChild($filterOptions->getCode() . '_filter', $block);
            }
        }

        $this->getLayer()->apply();

        return $this;
    }

    /* 
     * Filter values SQL for a set of attributes 
     */
    protected function filterValuesSql($category_id, $searching, $search_product_ids, $exclude_attributes, $include_attributes, $attribute_filters) { 

        // attribute filter sql 
        $sqlfilters = array();
        foreach ($attribute_filters as $attribute_code => $attribute_values) if (count($attribute_values) > 0) 
            $sqlfilters[] = " 
            cp.product_id in (
                select eavf.entity_id 
                from catalog_product_index_eav eavf 
                join eav_attribute af on eavf.attribute_id = af.attribute_id 
                where eavf.store_id = " . intval(Mage::app()->getStore()->getId()) . "
                and   af.attribute_code = '" . mysql_escape_string($attribute_code) . "' 
                and   eavf.value in (" . implode(',', $attribute_values) . ") 
            )";
        $filtersql = ' and ' . implode(' and ', $sqlfilters); 

        // return the main sql 
        return "
        select a.attribute_id, a.attribute_code, a.frontend_label, sf.position, eav.value as option_id, count(distinct cp.product_id) as count
        from eav_attribute a
        join catalog_product_index_eav eav on eav.attribute_id = a.attribute_id 
        join catalog_category_product_index cp on cp.product_id = eav.entity_id
        join catalog_product_index_price pr on pr.entity_id = eav.entity_id
        join m_filter2 f on f.code = a.attribute_code
        join m_filter2_store sf on sf.global_id = f.id
        where eav.store_id = " . intval(Mage::app()->getStore()->getId()) . "
        and   cp.store_id = " . intval(Mage::app()->getStore()->getId()) . "
        and   cp.visibility in (" . implode(',', Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds()) . ")
        and   cp.category_id = " . intval($category_id) . "
        and   pr.website_id = " . intval(Mage::app()->getStore()->getWebsiteId()) . " 
        and   pr.customer_group_id = 0
        and   sf.store_id = " . intval(Mage::app()->getStore()->getId()) . "
        and   sf.is_enabled = '1'
        " . ($searching ? " and sf.is_enabled_in_search = '1' " : "") . "
        " . ($searching ? " and cp.product_id in (" . implode(',', $search_product_ids) . ") " : "") . "
        " . (count($exclude_attributes) > 0 ? " and a.attribute_code not in ('" . implode("','", $exclude_attributes) . "') " : "") . "
        " . (count($include_attributes) > 0 ? " and a.attribute_code in ('" . implode("','", $include_attributes) . "') " : "") . "
        " . (count($attribute_filters) > 0 ? $filtersql : "") . " 
        group by a.attribute_id, a.attribute_code, eav.value
        ";
    }
    
    /* 
     * Main routine to retrieve filters data from database in one swoop 
     */
    public function getFiltersData($attributeCodeArray) {

        $search_product_ids = array();

        // request object 
        $request = Mage::app()->getFrontController()->getRequest();
        
        // if we are searching, we will filter by the shown products 
        $searching = $request->getModuleName() == 'catalogsearch';  
        if ($searching) { 

            // product ids that came up in search
            $search_product_ids = Mage::getSingleton('catalogsearch/layer')->getProductCollection()->getAllIds();
            
            // category id will be the root category (any visible product)  
            $category_id = Mage::app()->getStore()->getRootCategoryId();
        }
        else { 

            // currently selected category
            $current_category = Mage::registry('current_category');
            $category_id = $current_category->getId();
        }

        // get active filter attribute filters and values  
        $attribute_filters = array();
        foreach (Mage::getSingleton('catalog/layer')->getState()->getFilters() as $filter) { 
            $attribute_code = $filter->filter->getFilterOptions()->getCode(); 
            if (!isset($attribute_filters[$attribute_code])) $attribute_filters[$attribute_code] = array();
            $attribute_filters[$attribute_code][] = $filter->getData('value');
        }
        
        // selected attributes get filtered in a layered way, unselected attributes get filtered by all the selected attribute filters - start with selected attributes  
        $sql = '';
        $applied_attribute_filters = array(); 
        foreach ($attribute_filters as $attribute_code => $attribute_values) {
            $sql .= $this->filterValuesSql($category_id, $searching, $search_product_ids, array(), array($attribute_code), $applied_attribute_filters) . " \n union all \n ";
            $applied_attribute_filters[$attribute_code] = $attribute_values;
        } 

        // now add the other attributes 
        $sql .= $this->filterValuesSql($category_id, $searching, $search_product_ids, array_keys($applied_attribute_filters), array(), $applied_attribute_filters);
        
        // now order by position and label 
        $sql .= " order by position, frontend_label ";
        
        // this array contains a single array of values and counts - we need to group it into a double array for easier processing in the template 
        $attrValuesAndCounts = Mage::getSingleton('core/resource')->getConnection('core_read')->query($sql)->fetchAll();

        $filters = array();
        foreach ($attrValuesAndCounts as $attrValueAndCount) { 
            
            // attribute code, value and count
            $attrId = $attrValueAndCount['attribute_id'];
            $attrCode = $attrValueAndCount['attribute_code']; 
            $attrLabel = $attrValueAndCount['frontend_label'];
            $attrOption = $attrValueAndCount['option_id'];
            $attrValCount = $attrValueAndCount['count'];
            
            // if we have not encountered this attribute before, create the double array entry 
            if (!isset($filters[$attrCode])) $filters[$attrCode] = array('id' => $attrId, 'label' => $attrLabel, 'show_expanded' => false, 'values' => array());
            
            // add the value to the appropriate attribute 
            $filters[$attrCode]['values'][$attrOption] = array('count' => $attrValCount); 
        }

        // does this category have sub categories or not - if so, we will display the product grop options  
        $_isLastChildCategory = false;
        if ($request->getControllerName() != 'result' && $request->getModuleName() != 'catalogsearch' && !$current_category->hasChildren()) $_isLastChildCategory = true;

        // now eliminate some of the attributes based on business logic
        $i = 0; 
        foreach ($filters as $_attributeCode => &$_attributeData) {

            // skip the product group filter if it has more than 20 options and this category does not have sub categories  
            if ($_attributeCode == 'ln_product_group' && count($_attributeData['values']) > 20 && !$_isLastChildCategory) { unset($filters[$_attributeCode]); continue; }  
                        
            // we will show the first 3 attributes as expanded 
            if ($i++ < 3) $_attributeData['show_expanded'] = true;
            
            // if an explicit filter is given, we will show that attribute filter as well of course
            if (in_array($_attributeCode, $attributeCodeArray)) $_attributeData['show_expanded'] = true;
        }
        
        // get the value texts for the filters (option id => attribute id)  
        $option_ids = array();
        foreach ($filters as $_attributeCode => &$_attributeData) foreach ($_attributeData['values'] as $option_id => $option_count) $option_ids[$option_id] = array('attribute_id' => $_attributeData['id'], 'attribute_code' => $_attributeCode);

        // now get the value text for the options shown
        $valSql = "";
        foreach ($option_ids as $option_id => $option_data) $valSql .= " or (eao.attribute_id = " . intval($option_data['attribute_id']) . " and eao.option_id = " . intval($option_id) . " ) ";
        if ($valSql != "") { 

            // now complete the rest of the SQL (before we just did the filters) 
            $valSql = "
            select eao.option_id, if(aov.value_id > 0, aov.value, adv.value) as value
            from eav_attribute_option eao 
            join eav_attribute_option_value adv on adv.option_id = eao.option_id and adv.store_id = 0   
            left join eav_attribute_option_value aov on aov.option_id = eao.option_id and adv.store_id = " . intval(Mage::app()->getStore()->getId()) . "
            where 1 = 0 
            $valSql
            ";
            
            // now get the actual values for the options  
            $valuesData = Mage::getSingleton('core/resource')->getConnection('core_read')->query($valSql)->fetchAll();

            // now add the actual values for the options in the filters array
            foreach ($valuesData as $valueData) $filters[$option_ids[$valueData['option_id']]['attribute_code']]['values'][$valueData['option_id']]['value'] = $valueData['value']; 
        }

        // sort the option values for display 
        foreach ($filters as $_attributeCode => &$_attributeData) { 

            // we will not be sorting the attributes that are not expanded 
            // if (!$_attributeData['show_expanded']) continue;

            // now sort the values with custom logic 
            uasort($_attributeData['values'], function($a, $b) {
                
                // see if we can get a numeric value for both options
                $av = getFilterOptionNumericSortValue(trim($a['value']));
                $bv = getFilterOptionNumericSortValue(trim($b['value']));
                
                // numeric value ok - use it
                if ($av > 0 && $bv > 0) {
                    if ($av == $bv) return 0;
                    if ($av  < $bv) return -1;
                    return 1;
                }
                
                // numeric value not applicable - use label with natural string comparison
                return strnatcmp($a['value'], $b['value']);
            } );
        }
        
        // return the double array 
        return $filters;
    }

    public function getFilters() {
        $filters = array();
        foreach (Mage::helper('collisionsync_filters')->getFilterOptionsCollection() as $filterOptions) {
            if ($filterOptions->getIsEnabled()) {
                if (Mage::helper('collisionsync_filters')->canShowFilterInBlock($this, $filterOptions)) {
                    $filters[] = $this->getChild($filterOptions->getCode() . '_filter');
                }
            }
        }
        return $filters;
    }
    
    public function getClearUrl() {
        return Mage::helper('collisionsync_filters')->getClearUrl();
    }

    public function getLayer() {
        if (Mage::app()->getFrontController()->getRequest()->getModuleName() == 'catalogsearch') return Mage::getSingleton('catalogsearch/layer');
        else return Mage::getSingleton('catalog/layer');
    }
    
    public function canShowBlock() {
        if ($this->canShowOptions()) {
            return true;
        } elseif ($state = $this->getChild('layer_state')) {
            $appliedFilters = $this->getChild('layer_state')->getActiveFilters();
            return !empty($activeFilters);
        }
        else {
            return false;
        }
    }
}

/*
 * try to find a numeric sort value for the option
*/
function getFilterOptionNumericSortValue($str) {

    $int = 0;
    $float = 0;
    $result = str_replace(',', '', preg_replace('[^0-9]', '', $str));

    $str = str_replace(',', '', $str);

    if(is_float($str)){
        return (float)$str;
    }

    $parts = explode(' ', $str);

    $multiplier = 1;
    if (count($parts) == 2) {

        $measure = trim(strtolower($parts[1]));

        $parts = explode('-', $parts[0]);

        switch ( $measure )
        {
            case "in":
                $multiplier = 2.54;
                break;
            case "yd":
                $multiplier = 91.44;
                break;
            case "ft":
                $multiplier = 30.48;
                break;
            case "mm":
                $multiplier = 0.1;
                break;
            case "m":
                $multiplier = 100;
                break;
            default:
                $multiplier = 1;
        }

        if (count($parts) == 1) {
            $float_str = $parts[0];

            $explod = explode('/', $float_str);
            if( count($explod) > 1 ){
                list($top, $bottom) = explode('/', $float_str);
                $float = $top / $bottom;
                if ($float){
                    return (((float) $float) * $multiplier);
                }
                else{
                    return (((float) $float_str) * $multiplier);
                }
            }else{
                return (((float) $float_str) * $multiplier);
            }


        }
        if (count($parts) == 2) {
            $int = $parts[0];
            $float_str = $parts[1];
            $explod = explode('/', $float_str);
            if( count($explod) > 1 ){
                list($top, $bottom) = explode('/', $float_str);
                $float = $top / $bottom;
                return (((float) ($int + $float)) * $multiplier);
            }else{
                return (((float) ($int + $float)) * $multiplier);
            }

        }
    }

    return (((float) $str) * $multiplier);
}


