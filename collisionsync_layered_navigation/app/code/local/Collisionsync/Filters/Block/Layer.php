<?php
/** 
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * @author Collisionsync Team
 *
 */
class Collisionsync_Filters_Block_Layer extends Mage_Core_Block_Template {
	
	protected function _construct()
    {
        $this->addData(array('cache_lifetime'    => false, ));
    }

    public function getCacheTags()
    {
        return array(Mage_Catalog_Model_Product::CACHE_TAG);
    }

    public function getCacheKey()
    {
        $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
        return $this->getRequest()->getRequestUri();
    } 

    public function setCategoryId($id)
    {
        $category = Mage::getModel('catalog/category')->load($id);
        if ($category->getId()) {
            Mage::getSingleton('catalog/layer')->setCurrentCategory($category);
        }
    }
}