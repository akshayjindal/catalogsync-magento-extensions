<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* BASED ON SNIPPET: Resources/Install/upgrade script */
/* @var $installer Collisionsync_Filters_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->installEntities();
$installer->updateDefaultMaskFields(Collisionsync_Filters_Model_Filter::ENTITY);

$installer->endSetup();