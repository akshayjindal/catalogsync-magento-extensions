<?php

class Collisionsync_Filters_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	$category = Mage::getModel('catalog/category')->load(4);
        if ($category->getId()) {
            Mage::getSingleton('catalog/layer')->setCurrentCategory($category);
            print_r(Mage::getSingleton('catalog/layer'));
        }
    }
}
