<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Model type for holding information in memory about possible or applied filter which is based on an attribute
 * @author Collisionsync Team
 * Injected instead of standard catalog/layer_filter_attribute in Collisionsync_Filters_Block_Filter_Attribute constructor.
 */
class Collisionsync_Filters_Model_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute {
    /**
     * Apply attribute option filter to product collection
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Collisionsync_Filters_Block_Filter_Attribute $filterBlock
     * @return  Collisionsync_Filters_Model_Filter_Attribute
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     * @see app/code/core/Mage/Catalog/Model/Layer/Filter/Mage_Catalog_Model_Layer_Filter_Attribute::apply()
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = $request->getParam($this->_requestVar);
        if (is_array($filter)) {
            return $this;
        }
        
        //  BEGIN: when several filter options can be applied, several labels should be added to layer 
        // state, on label for each selected option. Here we assume all option ids to be in URL as one string value
        // separated by '_'
//        $text = $this->_getOptionText($filter);
        $text = array();
        foreach ($this->getMSelectedValues() as $optionId) {
            $text[$optionId] = $this->getAttributeModel()->getFrontend()->getOption($optionId);
        }
        //  END
        
        if ($filter && $text) {
            $this->_getResource()->applyFilterToCollection($this, $filter);
            //  BEGIN: create multiple items in layer state, prevent filter from hiding when value is set
            foreach ($this->getMSelectedValues() as $optionId) {
                $this->getLayer()->getState()->addFilter($this->_createItemEx(array(
                    'label' => $text[$optionId], 
                    'value' => $optionId,
                    'm_selected' => true,
                )));
            }
//            $this->_items = array();
            //  END
        }
        return $this;
    }
    /**
     * Creates in-memory representation of a single option of a filter
     * @param array $data
     * @return Collisionsync_Filters_Model_Item
     * This method is cloned from method _createItem() in parent class (method body was pasted from parent class 
     * completely rewritten.
     * Standard method did not give us possibility to initialize non-standard fields. 
     */
    protected function _createItemEx($data)
    {
        return Mage::getModel('collisionsync_filters/item')
            ->setData($data)
            ->setFilter($this);
    }
    /** 
     * Initializes internal array of in-memory representations of options of a filter
     * @return Collisionsync_Filters_Model_Filter_Attribute
     * @see Mage_Catalog_Model_Layer_Filter_Abstract::_initItems()
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     */
    protected function _initItems()
    {
        
        $data = $this->_getItemsData();
        $items = array();
        foreach ($data as $itemData) {
            //  BEGIN
            $items[] = $this->_createItemEx($itemData);
            //  END
        }
        //  BEGIN: enable additional filter item processing
        /* @var $ext Collisionsync_Filters_Helper_Extended */ $ext = Mage::helper(strtolower('Collisionsync_Filters/Extended'));
        $items = $ext->processFilterItems($this, $items);
        //  END
        $this->_items = $items;
        return $this;
    }
    /**
     * Returns all values currently selected for this filter
     */
    public function getMSelectedValues() {

        $values = Mage::helper('collisionsync_core')->sanitizeRequestNumberParam($this->_requestVar,
            array(array('sep' => '_', 'as_string' => true)));
        return $values ? array_filter(explode('_', $values)) : array();
    }
    
    /** 
     * Depending on current filter values and on attribute settings, returns available filter options from database
     * and additionally whether individual options are selected or not.
     * @return array
     * @see Mage_Catalog_Model_Layer_Filter_Attribute::_getItemsData()
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     */
    protected function _getItemsData()
    {
        $key = '';
        
        //  BEGIN: from url, retrieve ids of all options currently selected
        $selectedOptionIds = $this->getMSelectedValues();
        //  END
        $cache = Mage::getSingleton('core/cache');

        $replace = 'CUSTGROUP_'. Mage::getSingleton('customer/session')->getCustomerGroupId() .'_';

        $attribute = $this->getAttributeModel();
        $this->_requestVar = $attribute->getAttributeCode();

        //$key =  str_replace($replace, '', $this->getLayer()->getStateKey().'_'.$this->_requestVar);

        //$data = json_decode($cache->load($key), true);
        $data = null;
        //echo "<pre>";
        //print_r($data);
        //echo "</pre>";
        //$data = null;
        if ($data === null) {
            $options = $attribute->getFrontend()->getSelectOptions();
            $optionsCount = $this->_getResource()->getCount($this);
            $data = array();
            $sorter = array();

            foreach ($options as  $k => $option) {
                if (!$option || is_array($option['value'])) {
                    continue;
                }
                if (Mage::helper('core/string')->strlen($option['value'])) {
                    // Check filter type
                    if ($this->_getIsFilterableAttribute($attribute) == self::OPTIONS_ONLY_WITH_RESULTS) {
                        if (!empty($optionsCount[$option['value']]) || in_array($option['value'], $selectedOptionIds)) {
                            $sortValue = $this->calc(trim($option['label']));
                            if($sortValue && $sortValue > 0){
                                $sortValue = $sortValue;
                            }else{
                                $sortValue = $option['label'];
                            }
                            $sorter[$k] = $sortValue;
                            $data[] = array(
                                'label' => $option['label'],
                                'value' => $option['value'],
                                'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                                // mark each selected item now in memory so we could later mark it 
                                // visually in markup
                                'm_selected' => in_array($option['value'], $selectedOptionIds),
                                'sort' => $sortValue,
                                'code' => $attribute->getAttributeCode(),
                            );
                        }
                    }
                    else {
                        $sortValue = $this->calc(trim($option['label']));
                        if($sortValue && $sortValue > 0){
                            $sortValue = $sortValue;
                        }else{
                            $sortValue = $option['label'];
                        }
                        $sorter[$k] = $sortValue;
                        $data[] = array(
                            'label' => $option['label'],
                            'value' => $option['value'],
                            'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                            // mark each selected item now in memory so we could later mark it 
                            // visually in markup
                            'm_selected' => in_array($option['value'], $selectedOptionIds),
                            'sort' => $sortValue,
                            'code' => $attribute->getAttributeCode(),

                        );
                    }
                }
            }

            // $tags = array(
            //     Mage_Eav_Model_Entity_Attribute::CACHE_TAG.':'.$attribute->getId()
            // );

            // $tags = array();
            
            // sort($sorter, SORT_NUMERIC);

            // $data = $this->msort($data, array('sort'));

            $tags = array(
                Mage_Eav_Model_Entity_Attribute::CACHE_TAG.':'.$attribute->getId()
            );

            $tags = $this->getLayer()->getStateTags($tags);
            if ($sortMethod = $this->getFilterOptions()->getSortMethod()) {
                foreach ($data as $position => &$item) {
                    $item['position'] = $position;
                }
                usort($data, array(Mage::getSingleton('collisionsync_filters/sort'), $sortMethod));
            }

            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
                        
            // $cacheData = json_encode($data);
            // $cache->save( $cacheData, $key, $tags, 9999999999);
        }
        return $data;
    }

    private function msort( $data, $field ) {
        $field = (array) $field;
        uasort( $data, function($a, $b) use($field) {
            $retval = 0;
            foreach( $field as $fieldname ) {
                if( $retval == 0 ) $retval = strnatcmp( $a[$fieldname], $b[$fieldname] );
            }
            return $retval;
        } );
        return $data;
    }

    private function calc($str) {
        $int = 0;
        $float = 0;
        $result = str_replace(',', '', preg_replace('[^0-9]', '', $str));
        
        $str = str_replace(',', '', $str);

        if(is_float($str)){
            return (float)$str;
        }

        $parts = explode(' ', $str);

        $multiplier = 1;
        if (count($parts) == 2) {

            $measure = trim(strtolower($parts[1]));

            $parts = explode('-', $parts[0]);

            switch ( $measure )
            {
                case "in":
                    $multiplier = 2.54;
                    break;
                case "yd":
                    $multiplier = 91.44;
                    break;
                case "ft":
                    $multiplier = 30.48;
                    break;
                case "mm":
                    $multiplier = 0.1;
                    break;
                case "m":
                    $multiplier = 100;
                    break;
                default:
                    $multiplier = 1;
            } 

            if (count($parts) == 1) {
                $float_str = $parts[0];

                $explod = explode('/', $float_str);
                if( count($explod) > 1 ){
                    list($top, $bottom) = explode('/', $float_str);
                    $float = $top / $bottom;
                    if ($float){
                        return (((float) $float) * $multiplier);
                    }
                    else{
                        return (((float) $float_str) * $multiplier);
                    }
                }else{
                    return (((float) $float_str) * $multiplier);
                }
                
                
            }
            if (count($parts) == 2) {
                $int = $parts[0];
                $float_str = $parts[1];
                $explod = explode('/', $float_str);
                if( count($explod) > 1 ){
                    list($top, $bottom) = explode('/', $float_str);
                    $float = $top / $bottom;
                    return (((float) ($int + $float)) * $multiplier);
                }else{
                    return (((float) ($int + $float)) * $multiplier);
                }
                
            }
        }
        return (((float) $str) * $multiplier); 
    }

    
    /** 
     * This method locates resource type which should do all dirty job with the database. In this override, we 
     * instruct Magento to take our resource type, not standard. 
     * @see Mage_Catalog_Model_Layer_Filter_Attribute::_getResource()
     */
    protected function _getResource()
    {
        if (is_null($this->_resource)) {
            $this->_resource = Mage::getResourceModel((string)$this->getDisplayOptions()->resource);
        }
        return $this->_resource;
    }

    protected function _getIsFilterable()
    {
        switch ($this->getMode()) {
            case 'category': return $this->getFilterOptions()->getIsEnabled();
            case 'search': return $this->getFilterOptions()->getIsEnabledInSearch();
            default: throw new Exception('Not implemented');
        }
    }
    
    public function getRemoveUrl() {
        $query = array($this->getRequestVar()=>$this->getResetValue());
        $params = array('_secure' => Mage::app()->getFrontController()->getRequest()->isSecure());
        $params['_current']     = true;
        $params['_use_rewrite'] = true;
        $params['_m_escape'] = '';
        $params['_query']       = $query;
        return Mage::helper('collisionsync_filters')->markLayeredNavigationUrl(Mage::getUrl('*/*/*', $params), '*/*/*', $params);
    }
    protected function _getIsFilterableAttribute($attribute)
    {
        return $this->_getIsFilterable(); //return $this->getFilterOptions()->getIsEnabled();
    }
    public function getName() {
        return $this->getFilterOptions()->getName();
    }
}
