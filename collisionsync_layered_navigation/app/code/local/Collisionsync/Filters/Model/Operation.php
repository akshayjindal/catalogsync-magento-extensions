<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */
/**
 * @author Collisionsync Team
 *
 */
class Collisionsync_Filters_Model_Operation extends Collisionsync_Core_Model_Source_Abstract {
    protected function _getAllOptions() {
        return array(
            array('value' => '', 'label' => Mage::helper('collisionsync_filters')->__('Logical OR')),
            array('value' => 'and', 'label' => Mage::helper('collisionsync_filters')->__('Logical AND')),
        );
    }
}