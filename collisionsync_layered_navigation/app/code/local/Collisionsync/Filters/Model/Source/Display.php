<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base class for sources of filter templates
 * @author Collisionsync Team
 *
 */
class Collisionsync_Filters_Model_Source_Display extends Collisionsync_Core_Model_Source_Abstract {
	protected $_filterType = ''; // this should be filled in derived classes
	
	protected function _getAllOptions() {
		/* @var $core Collisionsync_Core_Helper_Data */ $core = Mage::helper(strtolower('Collisionsync_Core'));
		$result = array();
		
		foreach ($core->getSortedXmlChildren(Mage::getConfig()->getNode('collisionsync_filters/display'), $this->_filterType) as $key => $options) {
			$module = isset($options['module']) ? ((string)$options['module']) : 'collisionsyncpro_filteradmin'; 
    		$result[] = array('label' => Mage::helper($module)->__((string)$options->title), 'value' =>  $key);
		}
		return $result;
	}
	public function getDbType() {
		return 'varchar(255)';
	}
	public function getDbDefaultValue() {
		return '';
	}
}