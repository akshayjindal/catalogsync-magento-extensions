<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Filters
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Category of price filter templates
 * @author Collisionsync Team
 *
 */
class Collisionsync_Filters_Model_Source_Display_Price extends Collisionsync_Filters_Model_Source_Display {
	protected $_filterType = 'price';
}