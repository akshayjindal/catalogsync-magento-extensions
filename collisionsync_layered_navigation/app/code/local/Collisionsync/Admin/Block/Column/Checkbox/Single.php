<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.collisionsync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Block_Column_Checkbox_Single extends Collisionsync_Admin_Block_Column_Checkbox {
    public function renderCss() {
        return parent::renderCss() . ' ct-single';
    }
}