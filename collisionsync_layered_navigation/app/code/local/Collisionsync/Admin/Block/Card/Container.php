<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Block_Card_Container extends Collisionsync_Admin_Block_Crud_Card_Container {
    public function __construct() {
        parent::__construct();
        $model = Mage::registry('m_crud_model');
        $this->_headerText = $this->__('%s - Layered Navigation Filter', $model->getName());
    }
	protected function _prepareLayout() {
		$this->_addCloseButton()->_addApplyButton()->_addSaveButton();
	}
}