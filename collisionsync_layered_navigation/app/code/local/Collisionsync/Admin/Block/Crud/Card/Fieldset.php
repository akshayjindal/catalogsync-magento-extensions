<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.collisionsync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * FOR FUTURE USE
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Block_Crud_Card_Fieldset extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset {
    protected function _construct() {
        $this->setTemplate('collisionsync/admin/fieldset.phtml');
    }
}