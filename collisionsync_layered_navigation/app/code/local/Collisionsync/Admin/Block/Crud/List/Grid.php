<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.collisionsync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Block_Crud_List_Grid extends Collisionsync_Admin_Block_Crud_Grid {
    public function __construct() {
        parent::__construct();
        $this->setSaveParametersInSession(true);
    }
    public function getGridUrl() {
        return Mage::helper('collisionsync_admin')->getStoreUrl('*/*/grid');
    }

    public function getRowUrl($row) {
	    return Mage::helper('collisionsync_admin')->getStoreUrl('*/*/edit', array(
	    	'id' => Mage::helper('collisionsync_admin')->isGlobal() ? $row->getId() : $row->getGlobalId(),
	    ));
    }
}