<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.collisionsync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Enter description here ...
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Block_Helper_Wysiwyg extends Mage_Adminhtml_Block_Template {
    protected function _prepareLayout() {
        /* @var $js Collisionsync_Core_Helper_Js */ $js = Mage::helper(strtolower('Collisionsync_Core/Js'));
        /* @var $admin Collisionsync_Admin_Helper_Data */ $admin = Mage::helper(strtolower('Collisionsync_Admin'));
        $js->options('#collisionsync-wysiwyg-editor', array(
            'url' => $this->getUrl('*/catalog_product/wysiwyg'),
            'storeId' => $admin->getStore()->getId(),
        ));
        return parent::_prepareLayout();
    }


}