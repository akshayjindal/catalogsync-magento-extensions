<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.collisionsync.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_ChooserController extends Mage_Adminhtml_Controller_Action {
    public function productAction() {
        $this->getResponse()->setBody(Mage::helper('collisionsync_admin')->getProductChooserHtml());
    }
    public function cmsBlockAction() {
        $this->getResponse()->setBody(Mage::helper('collisionsync_admin')->getCmsBlockChooserHtml());
    }
}