<?php
/**
 * @category    Collisionsync
 * @package     Collisionsync_Admin
 * @copyright   Copyright (c) http://www.CollisionSync.com
 * @license     http://www.CollisionSync.com/license  Proprietary License
 */

/**
 * This controller contains actions for collisionsyncging layered navigation filters
 * @author Collisionsync Team
 *
 */
class Collisionsync_Admin_Collisionsync_FiltersController extends Collisionsync_Admin_Controller_Crud {
	protected function _getEntityName() {
		return 'collisionsync_filters/filter2';
	}
	/**
	 * Full page rendering action displaying list of entities of certain type. 
	 */
	public function indexAction() {
		// page
		$this->_title('Collisionsync')->_title($this->__('Layered Navigation Filters'));
        
		// layout
		$update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();

        if (!Mage::app()->isSingleStoreMode()) {
        	$update->addHandle('collisionsync_admin_multistore_list');
        }
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
		$this->_isLayoutLoaded = true;
        $this->_initLayoutMessages('adminhtml/session');

        // rendering
        $this->_setActiveMenu('collisionsync/filters');
        $this->renderLayout();
	}
	public function gridAction() {
		// layout
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
		$this->_isLayoutLoaded = true;

		// render AJAX result
		$this->renderLayout(); 
	}
	public function editAction() {
		$model = $this->_registerModel();

		// page
		$this->_title('Collisionsync')->_title($this->__('%s - Layered Navigation Filter', $model->getName()));
        
		// layout
		$update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();

        if (!Mage::app()->isSingleStoreMode()) {
        	$update->addHandle('collisionsync_admin_multistore_card');
        }
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
		$this->_isLayoutLoaded = true;
        $this->_initLayoutMessages('adminhtml/session');

        // simplify if one tab
		if (($tabs = $this->getLayout()->getBlock('tabs')) && count($tabs->getChild()) == 1) {
			$content = $tabs->getActiveTabBlock();
			$tabs->getParentBlock()->unsetChild('tabs');
			$this->getLayout()->getBlock('container')->insert($content, $content->getNameInLayout(), null, $content->getNameInLayout());
			$content->addToParentGroup('content');
		} 
		
        // rendering
		Mage::helper('collisionsync_core/js')->options('edit-form', array('editSessionId' => Mage::helper('collisionsync_db')->beginEditing()));
        $this->_setActiveMenu('collisionsync/filters');
        $this->renderLayout();
		
	}
	public function saveAction() {
		// data
		$fields = $this->getRequest()->getPost('fields');
        $useDefault = $this->getRequest()->getPost('use_default');
        $data = array();
		if (Mage::helper('collisionsync_admin')->isGlobal()) {
			$model = Mage::getModel('collisionsync_filters/filter2')->load($this->getRequest()->getParam('id'));
		}
		else {
			$model = Mage::getModel('collisionsync_filters/filter2_store')->loadByGlobalId($this->getRequest()->getParam('id'), 
				Mage::helper('collisionsync_admin')->getStore()->getId());
		}

        $response = new Varien_Object();
        $update = array();
        /* @var $messages Mage_Adminhtml_Block_Messages */ $messages = $this->getLayout()->createBlock('adminhtml/messages');

        try {
			// processing
			$model->addEditedData($fields, $useDefault);
            $model->addEditedDetails($this->getRequest());
			$model->validateKeys();
			Mage::helper('collisionsync_db')->replicateObject($model, array(
				$model->getEntityName() => array('saved' => array($model->getId()))
			));
			$model->validate();

			// do save
        	$model->save();
        	Mage::dispatchEvent('m_saved', array('object' => $model));
        	$messages->addSuccess($this->__('Your changes are successfully saved.'));
        }
        catch (Collisionsync_Db_Exception_Validation $e) {
        	foreach ($e->getErrors() as $error) {
        		$messages->addError($error);
        	}
        	$response->setError(true);
        }
        catch (Exception $e) {
        	$messages->addError($e->getMessage());
        	$response->setError(true);
        }
        
        $update[] = array('selector' => '#messages', 'html' => $messages->getGroupedHtml());
        $response->setUpdate($update);
        $this->getResponse()->setBody($response->toJson());
	}

	public function tabGeneralAction() {
		$model = $this->_registerModel();
		
		// layout
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
		$this->_isLayoutLoaded = true;

		// render AJAX result
		$this->renderLayout(); 
	}
}